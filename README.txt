This repository is used to produce Mass and Lifetime fits for Bd and B+ decays.
The source files are stored in /source
The input files are stored on eos, but you can produce RooDatasets that will be stored in /data to speed up the code
(during the first run set flag readRooData=false or copy from "/eos/user/r/ranovotn/public/BdNtuples/RooDatasetBd.root").
The RooDataset contain lifetime correction as weight!
The output images are stored in /run directory.
