 #include "RooRealVar.h"
 #include "RooDataSet.h"
 #include "RooDataHist.h"
 #include "RooGaussian.h"
 #include "RooGaussModel.h"
 #include "RooExponential.h"
 #include "TCanvas.h"
 #include "RooPlot.h"
 #include "TTree.h"
 #include "TFile.h"
 #include "TH1D.h"
 #include "TRandom.h"
 #include "RooAddPdf.h"
 #include "RooDecay.h"
 #include "RooProdPdf.h"
 #include "TH1F.h"
 #include "TH2F.h"
 #include "RooHistPdf.h"
 #include "RooFormulaVar.h"
 #include "RooProduct.h"
 #include "RooCategory.h"
 #include "TRandom3.h"
 #include "RooHist.h"
 #include "RooFitResult.h"

 #include "RooPDFs/RooTanhPdf.h"
 #include "RooPDFs/RooTanhPdf.cxx"

 #include "RooPDFs/RooDSCB.h"
 #include "RooPDFs/RooDSCB.cxx"

using namespace RooFit;

    Double_t massMin = 5000;
    Double_t massMax = 5600;

    Double_t tauMin = -2;
    Double_t tauMax = 30;

    Double_t tauErrMin = .018;
    Double_t tauErrMax = .165;

    Double_t massErrMin = 2;
    Double_t massErrMax = 100;

    Double_t ptMin = 10;
    Double_t ptMax = 100;

    set<TString> years {"2015", "2016","2016A", "2016B", "2016C", "2017", "2018"};
    
RooDataSet * readPeriod(TString year, bool useTriggers, RooCategory* yearsCategory){
    TFile *file = new TFile("/eos/user/r/ranovotn/public/BplusNtuplesNew/BplusFullRun2.root","READ");

    TTree* tree = (TTree*)file->Get("BplusBestChi");
    TTree* treeTrig = (TTree*)file->Get("BplusTriggers");
    tree->AddFriend(treeTrig);

    RooRealVar *mass = new RooRealVar("B_mass", "B_mass", massMin, massMax);
    RooRealVar *time = new RooRealVar("B_tau_MinA0", "B_tau_MinA0", tauMin, tauMax);
    RooRealVar *massErr = new RooRealVar("B_mass_err", "B_mass_err", massErrMin, massErrMax);
    RooRealVar *timeErr = new RooRealVar("B_tau_MinA0_err", "B_tau_MinA0_err", tauErrMin, tauErrMax);
    RooRealVar *pt = new RooRealVar("B_pT", "B_pT", ptMin, ptMax);

    TString newCuts = "";
    newCuts += "( ( ( abs(Jpsi_mu1_eta)<=1.05 && abs(Jpsi_mu2_eta)<=1.05 ) && ( Jpsi_mass>2959 && Jpsi_mass<3229 ) ) || ( ( abs(Jpsi_mu1_eta)>1.05 && abs(Jpsi_mu1_eta)<=2.5 && abs(Jpsi_mu2_eta)>1.05 && abs(Jpsi_mu2_eta)<=2.5 ) && ( Jpsi_mass>2852 && Jpsi_mass<3332 ) ) || ( ( ( abs(Jpsi_mu1_eta)<=1.05 && abs(Jpsi_mu2_eta)>1.05 && abs(Jpsi_mu2_eta)<=2.5 ) || ( abs(Jpsi_mu1_eta)>1.05 && abs(Jpsi_mu1_eta)<=2.5 && abs(Jpsi_mu2_eta)<=1.05 ) ) && ( Jpsi_mass>2913 && Jpsi_mass<3273 ) ) )";
    newCuts += Form("&& (B_tau_MinA0>%f && B_tau_MinA0<%f)",tauMin,tauMax);
    newCuts += Form("&& (B_tau_MinA0_err>%f && B_tau_MinA0_err<%f)",tauErrMin,tauErrMax);
    newCuts += Form("&& (B_mass>%f && B_mass<%f)",massMin,massMax);
    newCuts += Form("&& (B_mass_err>%f && B_mass_err<%f)",massErrMin,massErrMax);
    newCuts += "&& B_trk1_pT>3000";
    newCuts += "&& (B_mu1_pT>6000 && B_mu2_pT>6000)";
    newCuts += "&& pass_GRL";
    newCuts += "&& B_pT>10000 ";
    newCuts += "&& B_chi2_ndof<3.0  ";

    if(year == "2015"){
        newCuts += "&& (run_number<284700)";
        yearsCategory->setLabel("2015");
    }  else if(year == "2016A"){
        newCuts += "&& (run_number<302737 && run_number >= 296400)";
        yearsCategory->setLabel("2016A");
    } else if(year == "2016B"){
        newCuts += " && (run_number<302737 && run_number >= 296400)";
        yearsCategory->setLabel("2016B");
    } else if(year == "2016C"){
         newCuts += " && (run_number>=302737 && run_number<=311481)";
         yearsCategory->setLabel("2016C");
    } else if(year == "2017"){
            newCuts += "&& (run_number>=324320 && run_number < 341649)";
            yearsCategory->setLabel("2017");
    }  else if(year == "2018"){
            newCuts += "&& (run_number>=348197)";
            yearsCategory->setLabel("2018");
    }
    if(useTriggers){
        if(year == "2015"){
            newCuts += "&&  (HLT_2mu4_bJpsimumu_noL2 == 1 || HLT_mu6_mu4_bBmumuxv2 == 1 || HLT_mu18_2mu0noL1_JpsimumuFS == 1)";
        } else if(year == "2016A"){
             newCuts += "&& ((HLT_2mu6_bBmumuxv2 == 1) || (HLT_mu6_mu4_bBmumuxv2 == 1) || (HLT_mu10_mu6_bBmumuxv2 == 1))";
        } else if(year == "2016B"){
             newCuts += "&& ((HLT_2mu6_bBmumuxv2 == 0) && (HLT_mu6_mu4_bBmumuxv2 == 0) && (HLT_mu10_mu6_bBmumuxv2 == 0)) && (HLT_mu20_2mu0noL1_JpsimumuFS == 1 || HLT_mu10_mu6_bJpsimumu == 1 || HLT_mu6_mu4_bJpsimumu == 1)";
        } else if(year == "2016C"){
             newCuts += " &&  (HLT_2mu6_bJpsimumu_delayed == 1 || HLT_2mu6_bBmumuxv2_delayed == 1 || HLT_mu6_mu4_bBmumuxv2_delayed==1 || HLT_2mu4_bJpsimumu_L1BPH_2M8_2MU4 == 1 || HLT_mu6_mu4_bJpsimumu_delayed == 1 || HLT_2mu6_bBmumuxv2 == 1 || HLT_mu6_mu4_bJpsimumu == 1 || HLT_mu20_2mu4_JpsimumuL2 == 1)";
        } else if(year == "2017"){
            //newCuts += "&& ( HLT_2mu6_bJpsimumu_L1BPH_2M9_2MU6_BPH_2DR15_2MU6 == 1 || HLT_2mu6_bBmumuxv2_L1LFV_MU6 == 1 || HLT_mu11_mu6_bDimu == 1 || HLT_mu11_mu6_bBmumuxv2 == 1 || HLT_mu20_2mu2noL1_JpsimumuFS == 1 || HLT_mu6_mu4_bBmumuxv2 == 1 || HLT_2mu14 == 1 || HLT_mu50 == 1 )";
            newCuts += "&& ( HLT_2mu6_bJpsimumu_L1BPH_2M9_2MU6_BPH_2DR15_2MU6 == 1 || HLT_2mu6_bBmumuxv2_L1LFV_MU6 == 1 || HLT_mu11_mu6_bDimu == 1 || HLT_mu11_mu6_bBmumuxv2 == 1 || HLT_mu20_2mu2noL1_JpsimumuFS == 1 || HLT_mu6_mu4_bBmumuxv2 == 1 || HLT_2mu14 == 1 || HLT_mu50 == 1 || HLT_mu6_mu4_bBmumux_BsmumuPhi_L1BPH_2M9_MU6MU4_BPH_0DR15_MU6MU4 == 1 || HLT_2mu4_bBmumux_BsmumuPhi_L1BPH_2M9_2MU4_BPH_0DR15_2MU4 == 1 || HLT_mu22_mu8noL1 == 1 || HLT_mu11_2mu4noL1_bNocut_L1MU11_2MU6 == 1 || HLT_mu20_2mu4_JpsimumuL2 == 1 )";
        }  else if(year == "2018"){
            newCuts += "&& ( HLT_2mu6_bJpsimumu_L1BPH_2M9_2MU6_BPH_2DR15_2MU6 == 1 || HLT_2mu6_bBmumuxv2_L1LFV_MU6 == 1 || HLT_mu11_mu6_bDimu == 1 || HLT_mu11_mu6_bBmumuxv2 == 1 || HLT_mu20_2mu2noL1_JpsimumuFS == 1 || HLT_mu6_mu4_bBmumuxv2 == 1 || HLT_2mu14 == 1 || HLT_mu50 == 1 || HLT_mu6_mu4_bBmumux_BsmumuPhi_L1BPH_2M9_MU6MU4_BPH_0DR15_MU6MU4 == 1 || HLT_2mu4_bBmumux_BsmumuPhi_L1BPH_2M9_2MU4_BPH_0DR15_2MU4 == 1 || HLT_mu22_mu8noL1 == 1 || HLT_mu11_2mu4noL1_bNocut_L1MU11_2MU6 == 1 || HLT_mu20_2mu4_JpsimumuL2 == 1 )";
        }
    }
    cout << "Cuts used: "<< newCuts <<endl;

    Long64_t nEvents = tree->Draw("1", newCuts, "goff");
    tree->SetEstimate(nEvents);
    tree->Draw("B_mass:B_tau_MinA0:B_mass_err:B_tau_MinA0_err:B_pT/1000", newCuts, "para goff");

    RooAbsData::setDefaultStorageType(RooAbsData::Tree);
    RooArgSet*  vars = new RooArgSet(*mass, *time, *massErr, *timeErr, *pt,*yearsCategory);
    RooDataSet* data = new RooDataSet("data", newCuts, *vars);
    TIterator *vars_it = vars->createIterator();
    for ( Long64_t i = 0; i < nEvents; i++ ) {
        vars_it->Reset();
        Int_t j = 0;
        for ( auto var = (RooRealVar*)vars_it->Next(); var != NULL ; var = (RooRealVar*)vars_it->Next() ) {
            if(j!=vars->getSize()-1)var->setVal(tree->GetVal(j)[i]);
            j++;
        }
        data->add(*vars);
    }
//add weights
        TString tauWeight = "1.0";
        if(year == "2015"){ tauWeight ="(1./(0.274407*(1-0.421709*(TMath::Erf((@0-27.291111)/10.825437)+1))))/3.644778";
        } else if(year == "2016A"){tauWeight = "(1./(0.241034*(1-0.414092*(TMath::Erf((@0-8.870874)/9.252832)+1))))/4.473236";
        } else if(year == "2016B"){tauWeight = "(1./(0.088420*(1-0.444709*(TMath::Erf((@0--51.742706)/12.429203)+1))))/102.273208";
        } else if(year == "2016C"){tauWeight = "(1./(0.224168*(1-0.418187*(TMath::Erf((@0-26.470765)/10.425898)+1))))/4.461548";
        } else if(year == "2017"){tauWeight = "(1./(0.237452*(1-0.453662*(TMath::Erf((@0-27.049979)/12.475258)+1))))/4.215516";
        }  else if(year == "2018"){tauWeight = "(1./(0.237022*(1-0.472117*(TMath::Erf((@0-27.725697)/13.288633)+1))))/4.225342";  
        }
        RooFormulaVar wFunc("w","event tau-weight",tauWeight,*time) ;
        RooRealVar* w = (RooRealVar*) data->addColumn(wFunc) ;

    cout << year<<": Total number of events:" <<data->numEntries() <<endl;
    return data;
}

RooDataSet * readData(bool useTriggers, RooCategory *yearsCategory){
    RooAbsData::setDefaultStorageType(RooAbsData::Tree);
    RooDataSet* wdata = NULL;
    RooDataSet* wdataTmp[6];
    cout << "Use trigger selection: "<< useTriggers <<endl;
    cout << "Loading 2015..."<<endl;
    wdataTmp[0]=readPeriod("2015",useTriggers,yearsCategory);
    cout << "Loading 2016A..."<<endl;
    wdataTmp[1]=readPeriod("2016A",useTriggers,yearsCategory);
    cout << "Loading 2016B..."<<endl;
    wdataTmp[2]=readPeriod("2016B",useTriggers,yearsCategory);
    cout << "Loading 2016C..."<<endl;
    wdataTmp[3]=readPeriod("2016C",useTriggers,yearsCategory);
    cout << "Loading 2017..."<<endl;
    wdataTmp[4]=readPeriod("2017",useTriggers,yearsCategory);
    cout << "Loading 2018..."<<endl;
    wdataTmp[5]=readPeriod("2018",useTriggers,yearsCategory);

    wdata = wdataTmp[0];
    wdata->append(*wdataTmp[1]);
    wdata->append(*wdataTmp[2]);
    wdata->append(*wdataTmp[3]);
    wdata->append(*wdataTmp[4]);
    wdata->append(*wdataTmp[5]);
    ///save dataset to file
    TFile* f = new TFile("../data/RooDatasetBplus.root","RECREATE") ;
    wdata->Write();
    f->Close();

   return wdata;
}

void BplusMass(TString year = "",bool useWeight = false, bool readRooData = true,int sample = -1){
    RooCategory* yearsCategory = new RooCategory("yearsCategory","yearsCategory");
    yearsCategory->defineType("2015" ) ;
    yearsCategory->defineType("2016A") ;
    yearsCategory->defineType("2016B") ;
    yearsCategory->defineType("2016C") ;
    yearsCategory->defineType("2017" ) ;
    yearsCategory->defineType("2018" ) ;

    bool useTriggers = true;
    bool useMassPCE = true;

    bool sigRangePunzi =true;
    bool backgroundPunziSubtract =false;

    Double_t sidebandCutoff = 180;
    Double_t signaCutoff = 50;

    int nBins = 40;
    int nCPU = 3;
    //READ the data from file and put them to the RooDataset
    RooFitResult *result;
    RooRealVar *mass = new RooRealVar("B_mass", "B_mass", massMin, massMax);
    RooRealVar *time = new RooRealVar("B_tau_MinA0", "B_tau_MinA0", tauMin, tauMax);
    RooRealVar *massErr = new RooRealVar("B_mass_err", "B_mass_err", massErrMin, massErrMax);
    RooRealVar *timeErr = new RooRealVar("B_tau_MinA0_err", "B_tau_MinA0_err", tauErrMin, tauErrMax);
    RooRealVar *pt = new RooRealVar("B_pT", "B_pT", ptMin, ptMax);
    RooRealVar *weight = new RooRealVar("w", "weight", 0, 100);
    RooArgSet*  vars = new RooArgSet(*mass, *time, *massErr, *timeErr, *pt,*yearsCategory,*weight);


    RooDataSet* wdata = NULL;    
    RooDataSet* dataTmp = NULL;    
    RooDataSet* data = NULL;    
    if(!readRooData){
        dataTmp = readData(useTriggers,yearsCategory);
    }
    TFile* f = new TFile("../data/RooDatasetBplus.root","READ") ;
    dataTmp = (RooDataSet*)f->Get("data"); 

    if(years.find(year) != years.end()){
        data = (RooDataSet*) dataTmp->reduce(*vars,Form("yearsCategory==yearsCategory::%s",year.Data())) ;
    }else if(year == "2016"){
        data=(RooDataSet*) dataTmp->reduce(*vars,"yearsCategory==yearsCategory::2016A || yearsCategory==yearsCategory::2016C") ;
    }
    else{
        //data=(RooDataSet*) dataTmp->reduce(*vars,"yearsCategory==yearsCategory::2015 || yearsCategory==yearsCategory::2016A|| yearsCategory==yearsCategory::2016B|| yearsCategory==yearsCategory::2016C || yearsCategory==yearsCategory::2017 || yearsCategory==yearsCategory::2018") ;
       data=(RooDataSet*) dataTmp->reduce(*vars,"yearsCategory==yearsCategory::2015 || yearsCategory==yearsCategory::2016A || yearsCategory==yearsCategory::2016C || yearsCategory==yearsCategory::2017 || yearsCategory==yearsCategory::2018") ;
    }
    data->Print();

    if(useWeight){
        wdata = new RooDataSet(data->GetName(),data->GetTitle(),data,*data->get(),0,"w") ;
    }
    else wdata = data;
    if (sample > 0) {
    Long64_t nOrig = wdata->numEntries();
    TString title = wdata->GetTitle();
    title += " Random sample ( ";
    title += sample;
    title += " / ";
    title += nOrig;
    title += " )";
    RooArgSet*  vars = new RooArgSet(*mass, *time, *massErr, *timeErr, *pt,*yearsCategory);
    RooDataSet *newDataset = new RooDataSet("wdataRandomSample", title, *vars);
    TRandom3 rand(0);
    for ( Long64_t i = 0; i < sample; i++ ) {
        Long64_t j = rand.Uniform( 0 , nOrig );
        newDataset->add( *(wdata->get( j )) );
    }
    cout << "INFO: Created random sample (" << sample << " from " << nOrig << ")" << endl;
    wdata=newDataset;

    }
    
    ///Prepare model
    ////Signal
    //Mass
    RooRealVar *mass_mean      = new RooRealVar ("massSignal_mean", "Mean of Gaussian", 5279,5200, 5350);
    RooRealVar *mass_sigma1    = new RooRealVar ("massSignal_sigma1", "Sigma1 of DoubleGaussian", 20, 1, 30);
    RooRealVar *mass_sigma2    = new RooRealVar ("massSignal_sigma2", "Sigma2 of DoubleGaussian",40, 1, 50);
    RooRealVar *mass_fraction  = new RooRealVar ("massSignal_fraction", "Fraction of DoubleGaussian",0.5, 0., 1.);
    RooGaussian *mass_gauss1   = new RooGaussian("massSignal_gauss1", "Gaussian 1", *mass, *mass_mean, *mass_sigma1);
    RooGaussian *mass_gauss2   = new RooGaussian("massSignal_gauss2", "Gaussian 2", *mass, *mass_mean, *mass_sigma2);
    RooAddPdf* mass_gauss = new RooAddPdf  ("mass_gauss", "Mass Signal DoubleGaussian", RooArgList(*mass_gauss1, *mass_gauss2), *mass_fraction, kTRUE);

    RooRealVar *mass_meanMR       = new RooRealVar ("massSignal_meanMR", "", 5358.577);
    RooRealVar *mass_sigmaMR      = new RooRealVar ("massSignal_sigmaMR", "", 51.873);
    RooRealVar *mass_alfaLMR      = new RooRealVar ("massSignal_alfaLMR", "", 1.4586 );
    RooRealVar *mass_alfaRMR      = new RooRealVar ("massSignal_alfaRMR", "", 1.2684);
    RooRealVar *mass_nMR          = new RooRealVar ("massSignal_nMR", "",  6.171);

    RooDSCB *mass_missReco = new RooDSCB("massSignal_missReco","",*mass,*mass_meanMR,*mass_sigmaMR,*mass_alfaLMR,*mass_alfaRMR,*mass_nMR);

     RooRealVar *mass_fractionMR  = new RooRealVar ("massSignal_fractionMR", "Fraction of DoubleGaussian",0.0369);

RooAddPdf* massSignalPDF = new RooAddPdf  ("massSignalPDF", "Mass Signal DoubleGaussian", RooArgList(*mass_missReco,*mass_gauss), *mass_fractionMR, kTRUE);


    ////Background
    //Mass
    //Mass
    RooRealVar *simpleMass_scale  = new RooRealVar    ("simpleMass_scale", "Scale of Tanh",-0.0179206,-0.18,-0.0018);
    RooRealVar *simpleMass_offset = new RooRealVar    ("simpleMass_offset", "Offset of Tanh", 5141.70,5141.70-10,5141.70+10);
    RooTanhPdf *simpleMass_tanh   = new RooTanhPdf    ("simpleMass_tanh", "Tanh", *mass, *simpleMass_scale, *simpleMass_offset);

    RooRealVar *simpleMass_slope1     = new RooRealVar    ("simpleMass_slope1", "Slope of Exponential1",-8.83178e-04, -1, -1e-8);
    RooRealVar *simpleMass_slope2     = new RooRealVar    ("simpleMass_slope2", "Slope of Exponential2",-9.83178e-04, -1, -1e-8);
    RooExponential *simpleMass_exponential1 = new RooExponential("simpleMass_exponential1", "mass_exponential1", *mass, *simpleMass_slope1);
    RooExponential *simpleMass_exponential2 = new RooExponential("simpleMass_exponential2", "mass_exponential2", *mass, *simpleMass_slope2);
    RooRealVar *simpleMassBck_fraction  = new RooRealVar ("simpleMassBck_fraction", "Fraction of DoubleGaussian",0.95, 0., 0.99);

    //RooAddPdf* simpleMassBckPDF = new RooAddPdf  ("simpleMassBckPDF", "Mass Background", RooArgList(*simpleMass_exponential1, *simpleMass_exponential2),*simpleMassBck_fraction);
    RooAddPdf* simpleMassBckPDF = new RooAddPdf  ("simpleMassBckPDF", "Mass Background ", RooArgList(*simpleMass_exponential1, *simpleMass_tanh),*simpleMassBck_fraction);

    //Total
    RooRealVar *mass_sigFrac  = new RooRealVar ("mass_sigFrac", "mass_sigFrac",0.31, 0.1, 0.5);
    RooAddPdf* simpleMassPDF = new RooAddPdf  ("massPDF", "massPDF", RooArgList(*massSignalPDF, *simpleMassBckPDF), *mass_sigFrac, kTRUE);

    //standalone fit
    simpleMassPDF->fitTo(*wdata,RooFit::InitialHesse(kTRUE),RooFit::SumW2Error(kTRUE),NumCPU(nCPU),Timer(kTRUE));
    //Sideband subtraction to prepare punzi

    RooAddPdf* totPDF;
    RooAddPdf *totPunziPdfMassErr;
    if(useMassPCE){
    mass->setRange("signal",mass_mean->getValV()-signaCutoff,mass_mean->getValV()+signaCutoff);
    mass->setRange("leftSideband",massMin,mass_mean->getValV()-sidebandCutoff);
   
    RooAbsReal* fsigRegion_model = simpleMassPDF->createIntegral(*mass,NormSet(*mass),Range("signal")); 
    RooAbsReal* fsigRegion_sig = massSignalPDF->createIntegral(*mass,NormSet(*mass),Range("signal")); 

    RooAbsReal* fsigRegion_bck1 = simpleMass_exponential1->createIntegral(*mass,NormSet(*mass),Range("signal")); 
    RooAbsReal* fsigRegion_bck2 = simpleMass_tanh->createIntegral(*mass,NormSet(*mass),Range("signal")); 

    RooAbsReal* fbckRegion_bck1 = simpleMass_exponential1->createIntegral(*mass,NormSet(*mass),Range("leftSideband")); 
    RooAbsReal* fbckRegion_bck2 = simpleMass_tanh->createIntegral(*mass,NormSet(*mass),Range("leftSideband")); 

    Double_t fsig;
    Double_t fbck1;
    Double_t fbck2;
    if(sigRangePunzi){
        fsig = mass_sigFrac->getValV()*fsigRegion_sig->getValV()/fsigRegion_model->getValV();
        fbck1 = simpleMassBck_fraction->getValV()*fsigRegion_bck1->getValV();
        fbck2 = (1-simpleMassBck_fraction->getValV())*fsigRegion_bck2->getValV();
    }else{
        fsig = mass_sigFrac->getValV();
        fbck1 = (1-mass_sigFrac->getValV())*simpleMassBck_fraction->getValV();
        fbck2 = (1-mass_sigFrac->getValV())*(1-simpleMassBck_fraction->getValV());
    }
    Double_t fbckSideband = simpleMassBck_fraction->getValV()*fbckRegion_bck1->getValV()/(simpleMassBck_fraction->getValV()*fbckRegion_bck1->getValV()+(1-simpleMassBck_fraction->getValV())*fbckRegion_bck2->getValV());
    cout <<"Fraction of exponential in left sideband: "<< fbckSideband<<endl;

    RooDataSet* dataSidebandBoth = (RooDataSet*)wdata->reduce(Form("B_mass<%f || B_mass>%f",mass_mean->getValV()-sidebandCutoff,mass_mean->getValV()+sidebandCutoff));
    dataSidebandBoth->SetName("dataSidebandBoth");
    RooDataSet* dataSidebandLeft = (RooDataSet*)wdata->reduce(Form("B_mass<%f",mass_mean->getValV()-sidebandCutoff));
    dataSidebandLeft->SetName("dataSidebandLeft");
    RooDataSet* dataSidebandRight = (RooDataSet*)wdata->reduce(Form("B_mass>%f",mass_mean->getValV()+sidebandCutoff));
    dataSidebandRight->SetName("dataSidebandRight");
    RooDataSet* dataSignalBand = (RooDataSet*)wdata->reduce(Form("B_mass>%f && B_mass<%f",mass_mean->getValV()-signaCutoff,mass_mean->getValV()+signaCutoff));
    dataSignalBand->SetName("dataSignalBand");

    TH1F* signal_histMassErr     = (TH1F*)dataSignalBand->createHistogram("signal_histMassErr",*massErr,Binning(nBins,massErrMin,massErrMax));
    TH1F* background_histMassErrLeft = (TH1F*)dataSidebandLeft->createHistogram("background_histMassErrLeft",*massErr,Binning(nBins,massErrMin,massErrMax));
    TH1F* background_histMassErrRight = (TH1F*)dataSidebandRight->createHistogram("background_histMassErrRight",*massErr,Binning(nBins,massErrMin,massErrMax));
    TH1F* background_histMassErrBoth = (TH1F*)dataSidebandBoth->createHistogram("background_histMassErrBoth",*massErr,Binning(nBins,massErrMin,massErrMax));
    signal_histMassErr->Scale(1.0/signal_histMassErr->Integral(), "width");
    background_histMassErrLeft->Scale(1.0/background_histMassErrLeft->Integral(), "width");
    background_histMassErrRight->Scale(1.0/background_histMassErrRight->Integral(), "width");
    background_histMassErrBoth->Scale(1.0/background_histMassErrBoth->Integral(), "width");
    
    TH1F* background_histMassErrCombinatorial;
    TH1F* background_histMassErrMissReco;
    
    if(backgroundPunziSubtract){
    background_histMassErrLeft->Add(background_histMassErrRight,-1*fbckSideband);
    background_histMassErrLeft->Scale(1.0/background_histMassErrLeft->Integral(), "width");
    signal_histMassErr->Add(background_histMassErrLeft,-1*(1-fsig)*fbck2);
    signal_histMassErr->Add(background_histMassErrRight,-1*(1-fsig)*fbck1);
    background_histMassErrCombinatorial = background_histMassErrRight;
    background_histMassErrMissReco = background_histMassErrLeft;
    }else{
        signal_histMassErr->Add(background_histMassErrLeft,-1*(1-fsig)*fbck2);
        signal_histMassErr->Add(background_histMassErrBoth,-1*(1-fsig)*fbck1);
        background_histMassErrCombinatorial = background_histMassErrBoth;
        background_histMassErrMissReco = background_histMassErrLeft;
    }

    RooDataHist* background_dataHistMassErrMissReco = new RooDataHist(" background_dataHistMassErrMissReco", " background_dataHist", *massErr, background_histMassErrMissReco);
    RooHistPdf* backgroundPunziPDFMassErrMissReco = new RooHistPdf("backgroundPunziPDFMassErrMissReco","backgroundPunziPDF", *massErr, *background_dataHistMassErrMissReco);

    RooDataHist* background_dataHistMassErrCombinatorial = new RooDataHist(" background_dataHistMassErrCombinatorial", " background_dataHist", *massErr, background_histMassErrCombinatorial);
    RooHistPdf* backgroundPunziPDFMassErrCombinatorial = new RooHistPdf("backgroundPunziPDFMassErrCombinatorial","backgroundPunziPDF", *massErr, *background_dataHistMassErrCombinatorial);

    RooDataHist* signal_dataHistMassErr = new RooDataHist(" signal_dataHistMassErr", " signal_dataHist", *massErr, signal_histMassErr);
    RooHistPdf* signalPunziPDFMassErr = new RooHistPdf("signalPunziPDFMassErr","signalPunziPDF", *massErr, *signal_dataHistMassErr);

    totPunziPdfMassErr = new RooAddPdf  ("totPunziPdfMassErr", "total punzi pdf", RooArgList(*signalPunziPDFMassErr, *backgroundPunziPDFMassErrCombinatorial,*backgroundPunziPDFMassErrMissReco),RooArgList(*mass_sigFrac,*simpleMassBck_fraction), kTRUE);

//Mass Resolution
    RooRealVar *massPCE_mean     = new RooRealVar   ("massPCE_mean", "Mean (0.0) of Mass Resolution", mass_mean->getValV(),5250,5300);
    RooRealVar *massPCE_SF  = new RooRealVar   ("massPCE_SF", "SF of Mass Error", 1.28, 0.1, 10.);
    RooProduct *massPCE_sigma       = new RooProduct("massPCE_sigma","massErr * Scale Factor", RooArgList(*massErr, *massPCE_SF));
    RooGaussian *massResolutionPDF  = new RooGaussian("massResolution", "Mass Resolution Gaussian (for both Sig and Bck)", *mass, *massPCE_mean,*massPCE_sigma);

    RooProdPdf* massSignalPDFCond = new RooProdPdf("massSignalPDFCond","massSignalPDFCond",*signalPunziPDFMassErr,Conditional(*massResolutionPDF,*mass)) ;
    RooProdPdf* massMissRecoPDFCond = new RooProdPdf("massMissRecoPDFCond","massMissRecoPDFCond",*signalPunziPDFMassErr,Conditional(*mass_missReco,*mass)) ;

    RooAddPdf* massSigPDF = new RooAddPdf  ("massSigPDF", "Mass Signal PDF", RooArgList(*massMissRecoPDFCond,*massSignalPDFCond),*mass_fractionMR, kTRUE);

    RooRealVar *mass_scale  = new RooRealVar    ("mass_scale", "Scale of Tanh",simpleMass_scale->getValV(),simpleMass_scale->getValV()-4*simpleMass_scale->getError(),simpleMass_scale->getValV()+4*simpleMass_scale->getError());
    RooRealVar *mass_offset = new RooRealVar    ("mass_offset", "Offset of Tanh",simpleMass_offset->getValV(), simpleMass_offset->getValV()-2*simpleMass_offset->getError(),simpleMass_offset->getValV()+2*simpleMass_offset->getError());
    
    RooTanhPdf *mass_tanh   = new RooTanhPdf    ("mass_tanh", "Tanh", *mass, *mass_scale, *mass_offset);

    RooRealVar *mass_slope1     = new RooRealVar    ("massBck_slope1", "Slope of Exponential1",simpleMass_slope1->getValV(), -1, -1e-8);
    RooRealVar *mass_slope2     = new RooRealVar    ("massBck_slope2", "Slope of Exponential2",simpleMass_slope2->getValV(), -1, -1e-8);
    RooExponential *mass_exponential1 = new RooExponential("mass_exponential1", "mass_exponential1", *mass, *mass_slope1);
    RooExponential *mass_exponential2 = new RooExponential("mass_exponential2", "mass_exponential2", *mass, *mass_slope2);
    RooRealVar *massBck_fraction  = new RooRealVar ("massBck_fraction", "Fraction of DoubleGaussian",simpleMassBck_fraction->getValV(), 0., 1.);

    RooProdPdf* mass_exponential1_cond = new RooProdPdf("mass_exponential1_cond","mass_exponential1_cond",*backgroundPunziPDFMassErrCombinatorial,Conditional(*mass_exponential1,*mass)) ;
    RooProdPdf* mass_tanh_cond = new RooProdPdf("mass_tanh_cond","mass_tanh_cond",*backgroundPunziPDFMassErrMissReco,Conditional(*mass_tanh,*mass));
    RooAddPdf* massBackgroundPDFCond = new RooAddPdf  ("massBackgroundPDFCond", "Mass Background DoubleGaussian", RooArgList(*mass_exponential1_cond, *mass_tanh_cond),*massBck_fraction);
    RooAddPdf* massBckPDF = new RooAddPdf  ("massBckPDF", "Mass Background DoubleGaussian", RooArgList(*mass_exponential1, *mass_tanh),*massBck_fraction);


    RooRealVar *sigFrac  = new RooRealVar ("sigFrac", "sigFrac",mass_sigFrac->getValV(), 0., 1.);
    totPDF = new RooAddPdf  ("totPDF", "totPDF", RooArgList(*massSigPDF, *massBackgroundPDFCond), *sigFrac, kTRUE);

    if(useWeight)result = totPDF->fitTo(*wdata,Save(),NumCPU(nCPU),SumW2Error(kTRUE));
    else result = totPDF->fitTo(*wdata,Save(),NumCPU(nCPU));
    }

    TCanvas *c;
    TPad* pads[8];
    if(useMassPCE){
        c = new TCanvas("c","",2*800,2*600);
            for(int i = 0;i<2;i++){
                for(int j = 0;j<2;j++){
                        pads[4*i+2*j]     = new TPad(Form("pad%d",4*i+2*j)  ,   "",0.5*j,1-0.5*i,0.5*(j+1),1-0.5*i-0.4);
                        pads[4*i+2*j+1]   = new TPad(Form("pad%d",4*i+2*j+1),   "",0.5*j,1-0.5*i-0.4,0.5*(j+1),1-0.5*(i+1));
                }
            }
        for(int i = 0;i<8;i++)pads[i]->Draw();
    }
    else {
        c = new TCanvas("c","",800,600);
        pads[0]= new TPad("pad0",   "",0,1,1,0.2);
        pads[1]= new TPad("pad1",   "",0,0.2,1,0);
        for(int i = 0;i<2;i++)pads[i]->Draw();
    }
    pads[0]->cd();
 
    RooPlot* simpleMassFrame    = (RooPlot*)mass->frame();
    wdata->plotOn(simpleMassFrame,DataError(RooAbsData::SumW2));
    simpleMassPDF->plotOn(simpleMassFrame,RooFit::Components("mass_gauss"),LineColor(2));
    simpleMassPDF->plotOn(simpleMassFrame,RooFit::Components("massSignal_missReco"),LineColor(6));
    simpleMassPDF->plotOn(simpleMassFrame,RooFit::Components("simpleMass_tanh"),LineColor(1));
    simpleMassPDF->plotOn(simpleMassFrame,RooFit::Components("simpleMass_exponential1"),LineColor(3));
    simpleMassPDF->plotOn(simpleMassFrame);
    simpleMassFrame->Draw();

    pads[1]->cd();
    RooHist* hpullSimpleMass = simpleMassFrame->pullHist() ;
    RooPlot* simpleMassFramePull = (RooPlot*)mass->frame() ;
    simpleMassFramePull->addPlotable(hpullSimpleMass,"P") ;
    simpleMassFramePull->Draw();

    if(useMassPCE){
    pads[2]->cd();
    RooPlot* massErrFrame = (RooPlot*)massErr->frame();
    wdata->plotOn(massErrFrame,DataError(RooAbsData::SumW2));
    totPunziPdfMassErr->plotOn(massErrFrame);
    totPunziPdfMassErr->plotOn(massErrFrame,RooFit::Components("signalPunziPDFMassErr"),LineColor(2));
    totPunziPdfMassErr->plotOn(massErrFrame,RooFit::Components("backgroundPunziPDFMassErrMissReco"),LineColor(1));
    totPunziPdfMassErr->plotOn(massErrFrame,RooFit::Components("backgroundPunziPDFMassErrCombinatorial"),LineColor(3));
    massErrFrame->Draw();
    pads[3]->cd();
    RooHist* hpullMassErr = massErrFrame->pullHist() ;
    RooPlot* massErrFramePull = (RooPlot*)massErr->frame() ;
    massErrFramePull->addPlotable(hpullMassErr,"P") ;
    massErrFramePull->Draw();

    pads[4]->cd();
    RooPlot* massFrame    = (RooPlot*)mass->frame();

    wdata->plotOn(massFrame,DataError(RooAbsData::SumW2));
    totPDF->plotOn(massFrame,RooFit::Components("massSignalPDFCond"),LineColor(2));
    totPDF->plotOn(massFrame,RooFit::Components("massMissRecoPDFCond"),LineColor(6));
    //totPDF->plotOn(massFrame,RooFit::Components("massBackgroundPDFCond"),LineColor(5));
    totPDF->plotOn(massFrame,RooFit::Components("mass_exponential1_cond"),LineColor(3));
    totPDF->plotOn(massFrame,RooFit::Components("mass_tanh_cond"),LineColor(1));
    totPDF->plotOn(massFrame);
    massFrame->Draw();

    pads[5]->cd();
    RooHist* hpullMass = massFrame->pullHist() ;
    RooPlot* massFramePull = (RooPlot*)mass->frame() ;
    massFramePull->addPlotable(hpullMass,"P") ;
    massFramePull->Draw();

    }

    result->Print();
    if(years.find(year) != years.end())c->SaveAs(Form("../run/BplusMass%s.png",year.Data()));
    else c->SaveAs("../run/BplusMassFullRun2.png");


    

}
