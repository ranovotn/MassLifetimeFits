using namespace RooFit;

void PlotBp(){
    
    TFile* resultsFile =  new TFile("../run/BplusMassLifetimeFitResults.root");
    RooWorkspace* workspace = (RooWorkspace*)resultsFile->Get("workspace");

    RooRealVar *mass =workspace->var("B_mass");
    RooRealVar *time =workspace->var("B_tau_MinA0");
    RooRealVar *timeErr =workspace->var("B_tau_MinA0_err");
    RooRealVar *pt =workspace->var("B_pT");

    RooArgSet*  vars = new RooArgSet(*mass, *time);

    RooAbsPdf *totPDF = workspace->pdf("totPDF");
    RooAbsPdf *signalPunziPDF = workspace->pdf("signalPunziPDF");
    RooAbsPdf *backgroundPunziPDF = workspace->pdf("backgroundPunziPDF");
    RooAbsPdf *JpsiXPunziPDF = workspace->pdf("JpsiXPunziPDF");

    RooRealVar *sigFrac =workspace->var("sigFrac");
    RooRealVar *bckFrac =workspace->var("bckFrac");
    
    RooAddPdf *totPunziPdf = new RooAddPdf  ("totPunziPdf", "total punzi pdf", RooArgList(*signalPunziPDF, *backgroundPunziPDF,*JpsiXPunziPDF),RooArgList(*sigFrac,*bckFrac), kTRUE);
    

    RooDataSet* data = (RooDataSet*)workspace->data("data");
    //RooDataSet* data = (RooDataSet*)workspace->data("wdataRandomSample");
    
    RooPlot* massFrame    = (RooPlot*)mass->frame();
    RooPlot* timeFrame = (RooPlot*)time->frame();
    time->setRange("zoom",-0.4,0.4);
    RooPlot* timeZoomFrame = (RooPlot*)time->frame(Range("zoom"));
    
    TCanvas *c = new TCanvas("c","",3*800,600);
    TPad* pads[6];
    for(int i = 0;i<3;i++){
        pads[i]   = new TPad(Form("pad%d",i),   "",i*0.3,1.0,0.3*(i+1),0.2);
        pads[3+i] = new TPad(Form("pad%d",3+i), "",i*0.3,0.2,0.3*(i+1),0.0);
    }
    for(int i = 0;i<6;i++)pads[i]->Draw();

    RooDataSet *expDataDterr = totPunziPdf->generate(RooArgSet(*timeErr,*pt), 100000);

    data->plotOn(massFrame,DataError(RooAbsData::SumW2));
    totPDF->plotOn(massFrame,RooFit::Components("signalPDF"),LineColor(2));
    totPDF->plotOn(massFrame,RooFit::Components("backgroundPDF"),LineColor(3));
    totPDF->plotOn(massFrame,RooFit::Components("mass_JpsiX"),LineColor(1));
    totPDF->plotOn(massFrame,RooFit::Components("massSignal_missReco"),LineColor(6));

    totPDF->plotOn(massFrame);
    data->plotOn(timeFrame,DataError(RooAbsData::SumW2));
    totPDF->plotOn(timeFrame,RooFit::Components("signalPDF"),LineColor(2), ProjWData(*expDataDterr, kTRUE));
    totPDF->plotOn(timeFrame,RooFit::Components("backgroundPDF"),LineColor(1), ProjWData(*expDataDterr, kTRUE));
    totPDF->plotOn(timeFrame,RooFit::Components("timeResolutionPDF_cond"),LineColor(14), ProjWData(*expDataDterr, kTRUE));
    totPDF->plotOn(timeFrame, ProjWData(*expDataDterr, kTRUE));
    data->plotOn(timeZoomFrame,DataError(RooAbsData::SumW2));
    totPDF->plotOn(timeZoomFrame,RooFit::Components("signalPDF"),LineColor(2), ProjWData(*expDataDterr, kTRUE));
    totPDF->plotOn(timeZoomFrame,RooFit::Components("backgroundPDF"),LineColor(1), ProjWData(*expDataDterr, kTRUE));
    totPDF->plotOn(timeZoomFrame,RooFit::Components("timeResolutionPDF_cond"),LineColor(14), ProjWData(*expDataDterr, kTRUE));
    totPDF->plotOn(timeZoomFrame, ProjWData(*expDataDterr, kTRUE));
    
    RooHist* hpullMass = massFrame->pullHist() ;
    RooPlot* massFramePull = (RooPlot*)mass->frame() ;
    massFramePull->addPlotable(hpullMass,"P") ;

    RooHist* hpullTime = timeFrame->pullHist() ;
    RooPlot* timeFramePull = (RooPlot*)time->frame() ;
    timeFramePull->addPlotable(hpullTime,"P") ;

    RooHist* hpullTimeZoom = timeZoomFrame->pullHist() ;
    RooPlot* timeZoomFramePull = (RooPlot*)time->frame(Range("zoom")) ;
    timeZoomFramePull->addPlotable(hpullTimeZoom,"P") ;

    RooGenericPdf* pol0Time = new RooGenericPdf("pol0Time","0",*time) ;
    RooGenericPdf* pol0Mass = new RooGenericPdf("pol0Mass","0",*mass) ;
    RooGenericPdf* pol0TimeZoom = new RooGenericPdf("pol0TimeZoom","0",*time) ;

    pol0Time->plotOn(timeFramePull,LineColor(kRed));
    pol0TimeZoom->plotOn(timeZoomFramePull,LineColor(kRed));
    pol0Mass->plotOn(massFramePull,LineColor(kRed));

    pads[0]->cd();
    massFrame->Draw();
    pads[1]->cd();
    pads[1]->SetLogy();
    timeFrame->SetMinimum(1.0);
    timeFrame->Draw();
    pads[2]->cd();
    //pads[2]->SetLogy();
    timeZoomFrame->SetMinimum(1.0);
    timeZoomFrame->Draw();
    pads[3]->cd();
    massFramePull->Draw();
    pads[4]->cd();
    timeFramePull->SetMaximum(4.0);
    timeFramePull->SetMinimum(-4.0);
    timeFramePull->Draw();
    pads[5]->cd();
    timeZoomFramePull->Draw();
    
}
