#ifndef __CINT__
#include "RooGlobalFunc.h"
#endif
#include "RooRealVar.h"
#include "RooStats/SPlot.h"
#include "RooDataSet.h"
#include "RooRealVar.h"
#include "RooGaussian.h"
#include "RooExponential.h"
#include "RooChebychev.h"
#include "RooAddPdf.h"
#include "RooArgSet.h"
#include "RooArgList.h"

#include "RooFormulaVar.h"

#include "RooWrapperPdf.h"
#include "RooProdPdf.h"
#include "RooAddition.h"
#include "RooProduct.h"
#include "TCanvas.h"
#include "RooAbsPdf.h"
#include "RooFit.h"
#include "RooFitResult.h"
#include "RooWorkspace.h"
#include "RooConstVar.h"


//R__LOAD_LIBRARY(../source/RooPDFs/RooTanhPdf.cxx++);
//R__LOAD_LIBRARY(../source/RooPDFs/RooDSCB.cxx++);


using namespace RooFit ;
using namespace RooStats ;


    Double_t massMin = 5000;
    Double_t massFitMin = 5050;
    Double_t massMax = 5600;

    Double_t tauMin = -2;
    Double_t tauMax = 18;

    Double_t tauErrMin = .018;
    Double_t tauErrMax = .165;

    Double_t massErrMin = 0;
    Double_t massErrMax = 100;

    Double_t ptMin = 0;
    Double_t ptMax = 100;


RooDataSet * readPeriod(TString year, bool useTriggers, RooCategory* yearsCategory){
    TFile *file = new TFile("/eos/user/r/ranovotn/public/BplusNtuplesNew/BplusFullRun2.root","READ");

    TTree* tree = (TTree*)file->Get("BplusBestChi");
    TTree* treeTrig = (TTree*)file->Get("BplusTriggers");
    tree->AddFriend(treeTrig);

    RooRealVar *mass = new RooRealVar("B_mass", "B_mass", massMin, massMax);
    RooRealVar *time = new RooRealVar("B_tau_MinA0", "B_tau_MinA0", tauMin, tauMax);
    RooRealVar *massErr = new RooRealVar("B_mass_err", "B_mass_err", massErrMin, massErrMax);
    RooRealVar *timeErr = new RooRealVar("B_tau_MinA0_err", "B_tau_MinA0_err", tauErrMin, tauErrMax);
    RooRealVar *pt = new RooRealVar("B_pT", "B_pT", ptMin, ptMax);

    TString newCuts = "";
    newCuts += "( ( ( abs(Jpsi_mu1_eta)<=1.05 && abs(Jpsi_mu2_eta)<=1.05 ) && ( Jpsi_mass>2959 && Jpsi_mass<3229 ) ) || ( ( abs(Jpsi_mu1_eta)>1.05 && abs(Jpsi_mu1_eta)<=2.5 && abs(Jpsi_mu2_eta)>1.05 && abs(Jpsi_mu2_eta)<=2.5 ) && ( Jpsi_mass>2852 && Jpsi_mass<3332 ) ) || ( ( ( abs(Jpsi_mu1_eta)<=1.05 && abs(Jpsi_mu2_eta)>1.05 && abs(Jpsi_mu2_eta)<=2.5 ) || ( abs(Jpsi_mu1_eta)>1.05 && abs(Jpsi_mu1_eta)<=2.5 && abs(Jpsi_mu2_eta)<=1.05 ) ) && ( Jpsi_mass>2913 && Jpsi_mass<3273 ) ) )";
    newCuts += Form("&& (B_tau_MinA0>%f && B_tau_MinA0<%f)",tauMin,tauMax);
    newCuts += Form("&& (B_tau_MinA0_err>%f && B_tau_MinA0_err<%f)",tauErrMin,tauErrMax);
    newCuts += Form("&& (B_mass>%f && B_mass<%f)",massMin,massMax);
    newCuts += Form("&& (B_mass_err>%f && B_mass_err<%f)",massErrMin,massErrMax);
    newCuts += "&& B_trk1_pT>3000";
    newCuts += "&& (B_mu1_pT>6000 && B_mu2_pT>6000)";
    newCuts += "&& pass_GRL";
    newCuts += "&& B_pT>10000 ";
    newCuts += "&& B_chi2_ndof<3.0  ";

    if(year == "2015"){
        newCuts += "&& (run_number<284700)";
        yearsCategory->setLabel("2015");
    }  else if(year == "2016A"){
        newCuts += "&& (run_number<302737 && run_number >= 296400)";
        yearsCategory->setLabel("2016A");
    } else if(year == "2016B"){
        newCuts += " && (run_number<302737 && run_number >= 296400)";
        yearsCategory->setLabel("2016B");
    } else if(year == "2016C"){
         newCuts += " && (run_number>=302737 && run_number<=311481)";
         yearsCategory->setLabel("2016C");
    } else if(year == "2017"){
            newCuts += "&& (run_number>=324320 && run_number < 341649)";
            yearsCategory->setLabel("2017");
    }  else if(year == "2018"){
            newCuts += "&& (run_number>=348197)";
            yearsCategory->setLabel("2018");
    }
    if(useTriggers){
        if(year == "2015"){
            newCuts += "&&  (HLT_2mu4_bJpsimumu_noL2 == 1 || HLT_mu6_mu4_bBmumuxv2 == 1 || HLT_mu18_2mu0noL1_JpsimumuFS == 1)";
        } else if(year == "2016A"){
             newCuts += "&& ((HLT_2mu6_bBmumuxv2 == 1) || (HLT_mu6_mu4_bBmumuxv2 == 1) || (HLT_mu10_mu6_bBmumuxv2 == 1))";
        } else if(year == "2016B"){
             newCuts += "&& ((HLT_2mu6_bBmumuxv2 == 0) && (HLT_mu6_mu4_bBmumuxv2 == 0) && (HLT_mu10_mu6_bBmumuxv2 == 0)) && (HLT_mu20_2mu0noL1_JpsimumuFS == 1 || HLT_mu10_mu6_bJpsimumu == 1 || HLT_mu6_mu4_bJpsimumu == 1)";
        } else if(year == "2016C"){
             newCuts += " &&  (HLT_2mu6_bJpsimumu_delayed == 1 || HLT_2mu6_bBmumuxv2_delayed == 1 || HLT_mu6_mu4_bBmumuxv2_delayed==1 || HLT_2mu4_bJpsimumu_L1BPH_2M8_2MU4 == 1 || HLT_mu6_mu4_bJpsimumu_delayed == 1 || HLT_2mu6_bBmumuxv2 == 1 || HLT_mu6_mu4_bJpsimumu == 1 || HLT_mu20_2mu4_JpsimumuL2 == 1)";
        } else if(year == "2017"){
            //newCuts += "&& ( HLT_2mu6_bJpsimumu_L1BPH_2M9_2MU6_BPH_2DR15_2MU6 == 1 || HLT_2mu6_bBmumuxv2_L1LFV_MU6 == 1 || HLT_mu11_mu6_bDimu == 1 || HLT_mu11_mu6_bBmumuxv2 == 1 || HLT_mu20_2mu2noL1_JpsimumuFS == 1 || HLT_mu6_mu4_bBmumuxv2 == 1 || HLT_2mu14 == 1 || HLT_mu50 == 1 )";
            newCuts += "&& ( HLT_2mu6_bJpsimumu_L1BPH_2M9_2MU6_BPH_2DR15_2MU6 == 1 || HLT_2mu6_bBmumuxv2_L1LFV_MU6 == 1 || HLT_mu11_mu6_bDimu == 1 || HLT_mu11_mu6_bBmumuxv2 == 1 || HLT_mu20_2mu2noL1_JpsimumuFS == 1 || HLT_mu6_mu4_bBmumuxv2 == 1 || HLT_2mu14 == 1 || HLT_mu50 == 1 || HLT_mu6_mu4_bBmumux_BsmumuPhi_L1BPH_2M9_MU6MU4_BPH_0DR15_MU6MU4 == 1 || HLT_2mu4_bBmumux_BsmumuPhi_L1BPH_2M9_2MU4_BPH_0DR15_2MU4 == 1 || HLT_mu22_mu8noL1 == 1 || HLT_mu11_2mu4noL1_bNocut_L1MU11_2MU6 == 1 || HLT_mu20_2mu4_JpsimumuL2 == 1 )";
        }  else if(year == "2018"){
            newCuts += "&& ( HLT_2mu6_bJpsimumu_L1BPH_2M9_2MU6_BPH_2DR15_2MU6 == 1 || HLT_2mu6_bBmumuxv2_L1LFV_MU6 == 1 || HLT_mu11_mu6_bDimu == 1 || HLT_mu11_mu6_bBmumuxv2 == 1 || HLT_mu20_2mu2noL1_JpsimumuFS == 1 || HLT_mu6_mu4_bBmumuxv2 == 1 || HLT_2mu14 == 1 || HLT_mu50 == 1 || HLT_mu6_mu4_bBmumux_BsmumuPhi_L1BPH_2M9_MU6MU4_BPH_0DR15_MU6MU4 == 1 || HLT_2mu4_bBmumux_BsmumuPhi_L1BPH_2M9_2MU4_BPH_0DR15_2MU4 == 1 || HLT_mu22_mu8noL1 == 1 || HLT_mu11_2mu4noL1_bNocut_L1MU11_2MU6 == 1 || HLT_mu20_2mu4_JpsimumuL2 == 1 )";
        }
    }
    cout << "Cuts used: "<< newCuts <<endl;

    Long64_t nEvents = tree->Draw("1", newCuts, "goff");
    tree->SetEstimate(nEvents);
    tree->Draw("B_mass:B_tau_MinA0:B_mass_err:B_tau_MinA0_err:B_pT/1000", newCuts, "para goff");

    RooAbsData::setDefaultStorageType(RooAbsData::Tree);
    RooArgSet*  vars = new RooArgSet(*mass, *time, *massErr, *timeErr, *pt,*yearsCategory);
    RooDataSet* data = new RooDataSet("data", newCuts, *vars);
    TIterator *vars_it = vars->createIterator();
    for ( Long64_t i = 0; i < nEvents; i++ ) {
        vars_it->Reset();
        Int_t j = 0;
        for ( auto var = (RooRealVar*)vars_it->Next(); var != NULL ; var = (RooRealVar*)vars_it->Next() ) {
            if(j!=vars->getSize()-1)var->setVal(tree->GetVal(j)[i]);
            j++;
        }
        data->add(*vars);
    }
//add weights
        TString tauWeight = "1.0";
        if(year == "2015"){ tauWeight ="(1./(0.274407*(1-0.421709*(TMath::Erf((@0-27.291111)/10.825437)+1))))/3.644778";
        } else if(year == "2016A"){tauWeight = "(1./(0.241034*(1-0.414092*(TMath::Erf((@0-8.870874)/9.252832)+1))))/4.473236";
        } else if(year == "2016B"){tauWeight = "(1./(0.088420*(1-0.444709*(TMath::Erf((@0--51.742706)/12.429203)+1))))/102.273208";
        } else if(year == "2016C"){tauWeight = "(1./(0.224168*(1-0.418187*(TMath::Erf((@0-26.470765)/10.425898)+1))))/4.461548";
        } else if(year == "2017"){tauWeight = "(1./(0.237452*(1-0.453662*(TMath::Erf((@0-27.049979)/12.475258)+1))))/4.215516";
        }  else if(year == "2018"){tauWeight = "(1./(0.237022*(1-0.472117*(TMath::Erf((@0-27.725697)/13.288633)+1))))/4.225342";  
        }
        RooFormulaVar wFunc("w","event tau-weight",tauWeight,*time) ;
        RooRealVar* w = (RooRealVar*) data->addColumn(wFunc) ;

    cout << year<<": Total number of events:" <<data->numEntries() <<endl;
    return data;
}


RooDataSet * readData(bool useTriggers, RooCategory *yearsCategory){
    RooAbsData::setDefaultStorageType(RooAbsData::Tree);
    RooDataSet* wdata = NULL;
    RooDataSet* wdataTmp[6];
    cout << "Use trigger selection: "<< useTriggers <<endl;
    cout << "Loading 2015..."<<endl;
    wdataTmp[0]=readPeriod("2015",useTriggers,yearsCategory);
    cout << "Loading 2016A..."<<endl;
    wdataTmp[1]=readPeriod("2016A",useTriggers,yearsCategory);
    cout << "Loading 2016B..."<<endl;
    wdataTmp[2]=readPeriod("2016B",useTriggers,yearsCategory);
    cout << "Loading 2016C..."<<endl;
    wdataTmp[3]=readPeriod("2016C",useTriggers,yearsCategory);
    cout << "Loading 2017..."<<endl;
    wdataTmp[4]=readPeriod("2017",useTriggers,yearsCategory);
    cout << "Loading 2018..."<<endl;
    wdataTmp[5]=readPeriod("2018",useTriggers,yearsCategory);

    wdata = wdataTmp[0];
    wdata->append(*wdataTmp[1]);
    wdata->append(*wdataTmp[2]);
    wdata->append(*wdataTmp[3]);
    wdata->append(*wdataTmp[4]);
    wdata->append(*wdataTmp[5]);
    ///save dataset to file
    TFile* f = new TFile("RooDatasetBplus.root","RECREATE") ;
    wdata->Write();
    f->Close();

   return wdata;
}




void splot_blus_punzis(bool readRooData = false){

    RooCategory* yearsCategory = new RooCategory("yearsCategory","yearsCategory");
    yearsCategory->defineType("2015" ) ;
    yearsCategory->defineType("2016A") ;
    yearsCategory->defineType("2016B") ;
    yearsCategory->defineType("2016C") ;
    yearsCategory->defineType("2017" ) ;
    yearsCategory->defineType("2018" ) ;

    RooDataSet* wdata = NULL;    
    RooDataSet* dataTmp = NULL;    
    RooDataSet* data = NULL; 

    RooRealVar *mass = new RooRealVar("B_mass", "B_mass", massMin, massMax);
    RooRealVar *time = new RooRealVar("B_tau_MinA0", "B_tau_MinA0", tauMin, tauMax);
    RooRealVar *massErr = new RooRealVar("B_mass_err", "B_mass_err", massErrMin, massErrMax);
    RooRealVar *timeErr = new RooRealVar("B_tau_MinA0_err", "B_tau_MinA0_err", tauErrMin, tauErrMax);
    RooRealVar *pt = new RooRealVar("B_pT", "B_pT", ptMin, ptMax);
    RooRealVar *weight = new RooRealVar("w", "weight", 0, 100);
    RooArgSet*  vars = new RooArgSet(*mass, *time, *massErr, *timeErr, *pt,*yearsCategory,*weight);
    RooArgSet*  varsToy = new RooArgSet(*mass, *time, *massErr, *timeErr, *pt,*yearsCategory);

    bool useTriggers = true;

    if(!readRooData){
        dataTmp = readData(useTriggers,yearsCategory);
    }
    TFile* f = new TFile("RooDatasetBplus.root","READ") ;
    dataTmp = (RooDataSet*)f->Get("data"); 


//    dataTmp = readData(useTriggers,yearsCategory);
    data=(RooDataSet*) dataTmp->reduce(*vars,"yearsCategory==yearsCategory::2015 || yearsCategory==yearsCategory::2016A || yearsCategory==yearsCategory::2016C || yearsCategory==yearsCategory::2017 || yearsCategory==yearsCategory::2018") ;

    RooRealVar *mass_mean      = new RooRealVar ("massSignal_mean", "Mean of Gaussian", 5279,5200, 5350);
    RooRealVar *mass_sigma1    = new RooRealVar ("massSignal_sigma1", "Sigma1 of DoubleGaussian", 29, 10, 35);
    RooRealVar *mass_sigma2    = new RooRealVar ("massSignal_sigma2", "Sigma2 of DoubleGaussian",69, 10, 70);
    RooRealVar *mass_fraction  = new RooRealVar ("massSignal_fraction", "Fraction of DoubleGaussian",4.90855e-01, 0., 1.);
    RooGaussian *mass_gauss1   = new RooGaussian("massSignal_gauss1", "Gaussian 1", *mass, *mass_mean, *mass_sigma1);
    RooGaussian *mass_gauss2   = new RooGaussian("massSignal_gauss2", "Gaussian 2", *mass, *mass_mean, *mass_sigma2);
//    RooAddPdf* mass_gauss = new RooAddPdf  ("mass_gauss", "Mass Signal DoubleGaussian", RooArgList(*mass_gauss1, *mass_gauss2), *mass_fraction, kTRUE);

    RooRealVar *mass_meanMR       = new RooRealVar ("massSignal_meanMR", "", 5358.577);
    RooRealVar *mass_sigmaMR      = new RooRealVar ("massSignal_sigmaMR", "", 51.873);
    RooRealVar *mass_alfaLMR      = new RooRealVar ("massSignal_alfaLMR", "", 1.4586 );
    RooRealVar *mass_alfaRMR      = new RooRealVar ("massSignal_alfaRMR", "", 1.2684);
    RooRealVar *mass_nMR          = new RooRealVar ("massSignal_nMR", "",  6.171);

//    RooDSCB *mass_missReco = new RooDSCB("mass_missReco","",*mass,*mass_meanMR,*mass_sigmaMR,*mass_alfaLMR,*mass_alfaRMR,*mass_nMR);

    RooRealVar *mass_fractionMR  = new RooRealVar ("massSignal_fractionMR", "Fraction of DoubleGaussian",0.0369);

    RooAddPdf* massSignalPDF = new RooAddPdf  ("mass_gauss", "Mass Signal DoubleGaussian", RooArgList(*mass_gauss1, *mass_gauss2), *mass_fraction, kTRUE);
//    RooAddPdf* massSignalPDF = new RooAddPdf  ("massSignalPDF", "Mass Signal DoubleGaussian", RooArgList(*mass_missReco,*mass_gauss), *mass_fractionMR, kTRUE);
    ////Background
    //Mass
    //Mass
    RooRealVar *simpleMass_scale  = new RooRealVar    ("simpleMass_scale", "Scale of Tanh",-2.68675e-02,-0.2,-0.01);
    RooRealVar *simpleMass_offset = new RooRealVar    ("simpleMass_offset", "Offset of Tanh", 5.12876e+03,5121.70-25,5121.70+25);
//    RooTanhPdf *simpleMass_JpsiX   = new RooTanhPdf    ("simpleMass_JpsiX", "JpisX mass", *mass, *simpleMass_scale, *simpleMass_offset);
    RooGenericPdf *simpleMass_JpsiX_gen = new RooGenericPdf("simpleMass_JpsiX_gen","simpleMass_JpsiX_gen","1+TMath::TanH(@2*(@0-@1))",RooArgList(*mass,*simpleMass_offset,*simpleMass_scale));
    RooFormulaVar *simpleMass_JpsiX_func = new RooFormulaVar("simpleMass_JpsiX_func","1+TMath::TanH(@2*(@0-@1))",RooArgList(*mass,*simpleMass_offset,*simpleMass_scale));
    RooWrapperPdf *simpleMass_JpsiX = new RooWrapperPdf("simpleMass_JpsiX","simpleMass_JpsiX",*simpleMass_JpsiX_func);

    RooRealVar *simpleMass_slope1     = new RooRealVar    ("simpleMass_slope1", "Slope of Exponential1",-8.75792e-04, -1., -1.e-4);
    RooExponential *simpleMass_CombinatorialBck = new RooExponential("simpleMass_CombinatorialBck", "CombinatorialBck", *mass, *simpleMass_slope1);

    //Total    
    RooRealVar *signalYield = new RooRealVar ("signalYield", "signalYield",3.93617e+06, 8e5, data->numEntries());
    RooRealVar *JpsiXYield  = new RooRealVar ("JpsiXYield" , "JpsiXYield" ,7.19527e+05, 4e5, data->numEntries());
    RooRealVar *bkgYield    = new RooRealVar ("bkgYield"   , "bkgYield"   ,0.6*data->numEntries(), 8e5, data->numEntries());

    RooAddPdf* simpleMassPDF = new RooAddPdf  ("massPDF", "massPDF", RooArgList(*massSignalPDF, *simpleMass_CombinatorialBck,*simpleMass_JpsiX_gen), RooArgSet(*signalYield,*bkgYield,*JpsiXYield));

    RooFitResult *resultSimple = simpleMassPDF->fitTo(*data,Extended(),RooFit::Strategy(2),RooFit::InitialHesse(kTRUE),RooFit::SumW2Error(kTRUE),NumCPU(8),Timer(kTRUE),Save());

//    signalYield->setVal(  signalYield->getValV() *data->numEntries());
//    signalYield->setError(signalYield->getError()*data->numEntries());
//    bkgYield   ->setVal(  bkgYield   ->getValV() *data->numEntries());
//    bkgYield   ->setError(bkgYield   ->getError()*data->numEntries());
//    JpsiXYield->setVal(data->numEntries() - bkgYield->getValV() - signalYield->getValV());
//    JpsiXYield->setError(sqrt(bkgYield->getError()*bkgYield->getError() + signalYield->getError()*signalYield->getError()));
 
    mass_mean        ->setConstant();
    mass_sigma1      ->setConstant();
    mass_sigma2      ->setConstant();
    mass_fraction    ->setConstant();
    mass_meanMR      ->setConstant();
    mass_sigmaMR     ->setConstant();
    mass_alfaLMR     ->setConstant(); 
    mass_alfaRMR     ->setConstant();
    mass_nMR         ->setConstant(); 
    mass_fractionMR  ->setConstant(); 
    simpleMass_scale ->setConstant();
    simpleMass_offset->setConstant();

    RooMsgService::instance().setSilentMode(true);

    RooStats::SPlot* sData = new RooStats::SPlot("sData","An SPlot",*data, simpleMassPDF,RooArgList(*signalYield,*bkgYield,*JpsiXYield) );

    std::cout << "Check SWeights:" << std::endl;
    std::cout << std::endl <<  "Yield of Sig is " << signalYield->getVal() << ".  From sWeights it is " << sData->GetYieldFromSWeight("signalYield") << std::endl;
    std::cout << "Yield of Bkg is " << bkgYield->getVal() << ".  From sWeights it is " << sData->GetYieldFromSWeight("bkgYield") << std::endl << std::endl;
    std::cout << "Yield of JpsiX is " << JpsiXYield->getVal() << ".  From sWeights it is " << sData->GetYieldFromSWeight("JpsiXYield") << std::endl << std::endl;

    static const TString sSig("_sig");
    static const TString sBkg("_bkg");
    static const TString sJpsiX("_jpsiX");

    RooDataSet * dataw_sig   = new RooDataSet(data->GetName()+sSig  ,data->GetTitle()+sSig  ,data,*data->get(),0,"signalYield_sw") ;
    RooDataSet * dataw_bkg   = new RooDataSet(data->GetName()+sBkg  ,data->GetTitle()+sBkg  ,data,*data->get(),0,"bkgYield_sw") ;
    RooDataSet * dataw_kpsiX = new RooDataSet(data->GetName()+sJpsiX,data->GetTitle()+sJpsiX,data,*data->get(),0,"JpsiXYield_sw") ;


    TCanvas* cdata = new TCanvas("sPlot","sPlot demo", 1600, 1200);
    cdata->Divide(3,2);

    cdata->cd(1);
    RooPlot* frame1 = mass->frame(Title("B^{0}_{s} invariant mass distribution")) ;
    data->plotOn(frame1,Name("data"),DataError(RooAbsData::SumW2) ) ;
    simpleMassPDF->plotOn(frame1, LineColor(kRed)) ;
    simpleMassPDF->plotOn(frame1,Components(*massSignalPDF),               LineStyle(kDashed), LineColor(kGreen+1))  ;
    simpleMassPDF->plotOn(frame1,Components(*simpleMass_CombinatorialBck), LineStyle(kDashed), LineColor(kBlue+1));
    simpleMassPDF->plotOn(frame1,Components(*simpleMass_JpsiX_gen),        LineStyle(kDashed), LineColor(kCyan+1));
    TLegend *leg1 = new TLegend(0.6, 0.8, 0.8, 0.7);
    leg1->SetFillColor(kWhite);
    leg1->SetLineColor(kWhite);
    leg1->SetBorderSize(0.0);
    leg1->AddEntry("data", "Data", "p");
    leg1->AddEntry("massPDF", "Total Fit", "l");
    leg1->AddEntry("mass_gauss", "Signal", "l");
    leg1->AddEntry("simpleMass_CombinatorialBck", "Comb. background", "l");
    leg1->AddEntry("simpleMass_JpsiX_gen", "JpsiX", "l");
    frame1->addObject(leg1);
    frame1->Draw();

    cdata->cd(4);
    time->setRange("zoom",-0.3,1.2);
    RooPlot* frame9 = (RooPlot*)time->frame(Range("zoom"));
    data       ->plotOn(frame9,DataError(RooAbsData::SumW2),MarkerColor(kBlack)) ;
    dataw_sig  ->plotOn(frame9,DataError(RooAbsData::SumW2),MarkerColor(kGreen+1)) ;
    dataw_bkg  ->plotOn(frame9,DataError(RooAbsData::SumW2),MarkerColor(kBlue+1) ) ;
    dataw_kpsiX->plotOn(frame9,DataError(RooAbsData::SumW2),MarkerColor(kCyan+1) ) ;
    gPad->SetLogy();
    frame9->SetMinimum(5.);
    frame9->SetName("Lifetime");
    frame9->SetTitle("Lifetime distribution - zoomed");
    TLegend *leg2 = new TLegend(0.6, 0.8, 0.8, 0.7);
    leg2->SetFillColor(kWhite);
    leg2->SetLineColor(kWhite);
    leg2->SetBorderSize(0.0);
    leg2->AddEntry(data->GetName()       , "data", "p");
    leg2->AddEntry(data->GetName()+sSig  , "signal", "p");
    leg2->AddEntry(data->GetName()+sBkg  , "comb. background", "p");
    leg2->AddEntry(data->GetName()+sJpsiX, "JpsiX", "p");
    frame9->addObject(leg2);
    frame9->Draw() ;



    cdata->cd(2);
    RooPlot* frame2 = timeErr->frame() ;
    data       ->plotOn(frame2,DataError(RooAbsData::SumW2),MarkerColor(kBlack)) ;
    dataw_sig  ->plotOn(frame2,DataError(RooAbsData::SumW2),MarkerColor(kGreen+1)) ;
    dataw_bkg  ->plotOn(frame2,DataError(RooAbsData::SumW2),MarkerColor(kBlue+1) ) ;
    dataw_kpsiX->plotOn(frame2,DataError(RooAbsData::SumW2),MarkerColor(kCyan+1) ) ;
    frame2->SetName("Time_error");
    frame2->SetTitle("Time error distribution");
    frame2->addObject(leg2);    
    frame2->Draw() ;


    cdata->cd(3);
    RooPlot* frame3 = time->frame() ;
    data       ->plotOn(frame3,DataError(RooAbsData::SumW2),MarkerColor(kBlack)) ;
    dataw_sig  ->plotOn(frame3,DataError(RooAbsData::SumW2),MarkerColor(kGreen+1)) ;
    dataw_bkg  ->plotOn(frame3,DataError(RooAbsData::SumW2),MarkerColor(kBlue+1) ) ;
    dataw_kpsiX->plotOn(frame3,DataError(RooAbsData::SumW2),MarkerColor(kCyan+1) ) ;
    gPad->SetLogy();
    frame3->SetMinimum(5e-2);
    frame3->SetName("Lifetime");
    frame3->SetTitle("Lifetime distribution");
    frame3->addObject(leg2);    
    frame3->Draw() ;

    cdata->cd(5);
    RooPlot* frame4 = pt->frame() ;
    data       ->plotOn(frame4,DataError(RooAbsData::SumW2),MarkerColor(kBlack)) ;
    dataw_sig  ->plotOn(frame4,DataError(RooAbsData::SumW2),MarkerColor(kGreen+1)) ;
    dataw_bkg  ->plotOn(frame4,DataError(RooAbsData::SumW2),MarkerColor(kBlue+1) ) ;
    dataw_kpsiX->plotOn(frame4,DataError(RooAbsData::SumW2),MarkerColor(kCyan+1) ) ;
    frame4->SetName("Transverse momentum ");
    frame4->SetTitle("p_{T} distribution");
    frame4->addObject(leg2);    
    frame4->Draw() ;
    
    cdata->cd(6);
    RooPlot* frame5 = massErr->frame() ;
    data       ->plotOn(frame5,DataError(RooAbsData::SumW2),MarkerColor(kBlack)) ;
    dataw_sig  ->plotOn(frame5,DataError(RooAbsData::SumW2),MarkerColor(kGreen+1)) ;
    dataw_bkg  ->plotOn(frame5,DataError(RooAbsData::SumW2),MarkerColor(kBlue+1) ) ;
    dataw_kpsiX->plotOn(frame5,DataError(RooAbsData::SumW2),MarkerColor(kCyan+1) ) ;
    frame5->SetName("Mass error ");
    frame5->SetTitle("Mass error distribution");
    frame5->addObject(leg2);    
    frame5->Draw() ;

    cdata->SaveAs("splot_for_punzi.eps");
    cdata->SaveAs("splot_for_punzi.png");
    cdata->SaveAs("splot_for_punzi.pdf");
    cdata->SaveAs("splot_for_punzi.root");


    TCanvas* cdata_norm = new TCanvas("sPlot_norm","sPlot demo", 1600, 1200);
    cdata_norm->Divide(2,2);
    cdata_norm->cd(1);
    RooPlot* frame_norm1 = time->frame() ;
    dataw_sig  ->plotOn(frame_norm1,DataError(RooAbsData::SumW2),MarkerColor(kGreen+1)) ;
    dataw_bkg  ->plotOn(frame_norm1,DataError(RooAbsData::SumW2),MarkerColor(kBlue+1),Rescale(signalYield->getVal()/bkgYield->getVal()) ) ;
    dataw_kpsiX->plotOn(frame_norm1,DataError(RooAbsData::SumW2),MarkerColor(kCyan+1),Rescale(signalYield->getVal()/JpsiXYield->getVal()) ) ;
    gPad->SetLogy();
    frame_norm1->SetMinimum(5e-2);
    frame_norm1->SetName("Lifetime");
    frame_norm1->SetTitle("Lifetime distribution");
    TLegend *leg3 = new TLegend(0.6, 0.8, 0.8, 0.7);
    leg3->SetFillColor(kWhite);
    leg3->SetLineColor(kWhite);
    leg3->SetBorderSize(0.0);
    leg3->AddEntry(data->GetName()+sSig  , "signal", "p");
    leg3->AddEntry(data->GetName()+sBkg  , "comb. background", "p");
    leg3->AddEntry(data->GetName()+sJpsiX, "JpsiX", "p");
    frame_norm1->addObject(leg3);
    frame_norm1->Draw() ;

    cdata_norm->cd(2);
    RooPlot* frame_norm2 = timeErr->frame() ;
    dataw_sig  ->plotOn(frame_norm2,DataError(RooAbsData::SumW2),MarkerColor(kGreen+1)) ;
    dataw_bkg  ->plotOn(frame_norm2,DataError(RooAbsData::SumW2),MarkerColor(kBlue+1),Rescale(signalYield->getVal()/bkgYield->getVal())  ) ;
    dataw_kpsiX->plotOn(frame_norm2,DataError(RooAbsData::SumW2),MarkerColor(kCyan+1),Rescale(signalYield->getVal()/JpsiXYield->getVal()) ) ;
    frame_norm2->SetName("Time_error");
    frame_norm2->addObject(leg3);
    frame_norm2->SetTitle("Time error distribution");
    frame_norm2->Draw() ;

    cdata_norm->cd(3);
    RooPlot* frame_norm4 = pt->frame() ;
    dataw_sig  ->plotOn(frame_norm4,DataError(RooAbsData::SumW2),MarkerColor(kGreen+1)) ;
    dataw_bkg  ->plotOn(frame_norm4,DataError(RooAbsData::SumW2),MarkerColor(kBlue+1),Rescale(signalYield->getVal()/bkgYield->getVal())  ) ;
    dataw_kpsiX->plotOn(frame_norm4,DataError(RooAbsData::SumW2),MarkerColor(kCyan+1),Rescale(signalYield->getVal()/JpsiXYield->getVal())  ) ;
    frame_norm4->SetName("Transverse momentum ");
    frame_norm4->SetTitle("p_{T} distribution");
    frame_norm4->addObject(leg3);
    frame_norm4->Draw() ;

    cdata_norm->cd(4);
    RooPlot* frame_norm5 = massErr->frame() ;
    dataw_sig  ->plotOn(frame_norm5,DataError(RooAbsData::SumW2),MarkerColor(kGreen+1)) ;
    dataw_bkg  ->plotOn(frame_norm5,DataError(RooAbsData::SumW2),MarkerColor(kBlue+1),Rescale(signalYield->getVal()/bkgYield->getVal())  ) ;
    dataw_kpsiX->plotOn(frame_norm5,DataError(RooAbsData::SumW2),MarkerColor(kCyan+1),Rescale(signalYield->getVal()/JpsiXYield->getVal())  ) ;
    frame_norm5->SetName("Mass error ");
    frame_norm5->SetTitle("Mass error distribution");
    frame_norm5->addObject(leg3);
    frame_norm5->Draw() ;
   
    cdata_norm->SaveAs("splot_for_punzi_norm.eps");
    cdata_norm->SaveAs("splot_for_punzi_norm.png");
    cdata_norm->SaveAs("splot_for_punzi_norm.pdf");
    cdata_norm->SaveAs("splot_for_punzi_norm.root");

    TFile* f_splot = new TFile("RooDataSet_splot_output.root","RECREATE") ;
    f_splot->cd();
    dataw_sig->Write();
    dataw_bkg->Write();
    dataw_kpsiX->Write();
    f_splot->Close();

}
