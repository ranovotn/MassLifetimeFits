 #include "RooRealVar.h"
 #include "RooDataSet.h"
 #include "RooDataHist.h"
 #include "RooGaussian.h"
 #include "RooGaussModel.h"
 #include "RooExponential.h"
 #include "TCanvas.h"
 #include "RooPlot.h"
 #include "TTree.h"
 #include "TFile.h"
 #include "TH1D.h"
 #include "TRandom.h"
 #include "RooAddPdf.h"
 #include "RooDecay.h"
 #include "RooProdPdf.h"
 #include "TH1F.h"
 #include "TH2F.h"
 #include "RooHistPdf.h"
 #include "RooFormulaVar.h"
 #include "RooProduct.h"
 #include "TRandom3.h"
 #include "RooHist.h"
 #include "RooFitResult.h"
 #include "RooCategory.h"
 #include "RooWorkspace.h"
#include "core/MyData.h"
#include "core/MyData.cxx"


using namespace RooFit;
   
void SignalFitBd(TString years = "", bool readRooData = false,bool useWeight = true,bool useTriggers = true,int sample = -1, bool toyMC = false){
     RooAbsReal::defaultIntegratorConfig()->setEpsRel(1.E-5);
    RooAbsReal::defaultIntegratorConfig()->getConfigSection("RooAdaptiveIntegratorND").setRealValue("maxEval2D", 5000);

    RooAbsReal::defaultIntegratorConfig()->getConfigSection("RooAdaptiveGaussKronrodIntegrator1D").setRealValue("maxSeg", 50);
    RooAbsReal::defaultIntegratorConfig()->getConfigSection("RooAdaptiveGaussKronrodIntegrator1D").setCatLabel("method", "15Points");

    TString year = "MC";
    RooWorkspace* workspace = new RooWorkspace("workspace",kTRUE) ;
    MyData *myData;
    if(years == "2015" || years == "2016C" || years == "2016B" || years == "2015+2016" ){
         myData = new MyData("myData","/eos/atlas/atlascerngroupdisk/phys-beauty/BLifetime/BdKstar/MC/PS_MC/BdPrescaleoutput1516_3.root");
    }
    else if(years == "2017"){
         myData = new MyData("myData","/eos/atlas/atlascerngroupdisk/phys-beauty/BLifetime/BdKstar/MC/PS_MC/BdPrescaleoutput17_3.root");
    }else if(years == "2018"){
         myData = new MyData("myData","/eos/atlas/atlascerngroupdisk/phys-beauty/BLifetime/BdKstar/MC/PS_MC/BdPrescaleoutput18_3.root");
    }else if(years == "2016A"){
                 myData = new MyData("myData","/eos/atlas/atlascerngroupdisk/phys-beauty/BLifetime/BdKstar/MC/PS_MC/BdPrescaleoutput1516Legacy_Short.root");
    }

    myData->mainTree = "datadumpAllC";
    //myData->mainTree = "datadump";
    myData->trigTree = "";
    myData->rooFitFile = "../data/RooDatasetBdMC.root";

    myData->massPDG = 5279.65;
    myData->massMin = 5000;
    myData->massMax = 5650;
    myData->tauMin = -1.0;
    myData->tauMax = 20;
    myData->tauErrMin = 0.01;
    myData->tauErrMax = 0.25;
    myData->massErrMin = -1e8;
    myData->massErrMax = 1e8;
    myData->ptMin = -1e8;
    myData->ptMax = 1e8;
    myData->wMin = -1e8;
    myData->wMax = 1e8;

    
    if(years == "2015"){
        myData->triggersMC = "mainTriggers2015";
        myData->tauCorrMC = "(1./(0.160552*(1-0.654738*(TMath::Erf((@0-29.969762)/15.070683)+1))))/6.248619";
    }
    else if(years == "2016A"){
        myData->triggersMC= "mainTriggers2016A";
        //myData->tauCorrMC = "(1./(0.166868*(1-0.419287*(TMath::Erf((@0-9.806596)/8.177943)+1))))/6.227540";
        myData->tauCorrMC =   "(1./(0.160758*(1-0.429633*(TMath::Erf((@0-9.926538)/8.963977)+1))))/6.550744";


    }
    else if(years == "2016B"){
        myData->triggersMC= "((HLT_2mu6_bBmumuxv2 == 0) && (HLT_mu6_mu4_bBmumuxv2 == 0) && (HLT_mu10_mu6_bBmumuxv2 == 0)) && (HLT_mu20_2mu0noL1_JpsimumuFS == 1 || HLT_mu10_mu6_bJpsimumu == 1 || HLT_mu6_mu4_bJpsimumu == 1)";
        myData->tauCorrMC = "(1./(0.033551*(1-0.438482*(TMath::Erf((@0--11051686859.594877)/1913.537036)+1))))/242.247150";

    }
    else if(years == "2016C"){
         myData->triggersMC= "mainTriggers2016B";
        myData->tauCorrMC = "(1./(0.101746*(1-0.723472*(TMath::Erf((@0-30.166242)/15.963276)+1))))/9.882206";
     }
    else if(years == "2017"){
         myData->triggersMC=  "mainTriggers2017";
         myData->tauCorrMC = "(1./(0.079517*(1-33.827372*(TMath::Erf((@0-91.287130)/39.062252)+1))))/12.993400";
    }
    else if(years == "2018"){
        myData->triggersMC=  "mainTriggers2018";
        //myData->tauCorrMC = "(1./(0.076598*(1-0.442158*(TMath::Erf((@0-23.248299)/12.092999)+1))))/13.093064";//orig
        myData->tauCorrMC = "(1./(0.076658*(1-0.513180*(TMath::Erf((@0-24.738182)/12.913618)+1))))/13.090343";


    }else if(years == "2015+2016"){
         myData->triggersMC= "(mainTriggers2016B==1 || mainTriggers2015 == 1)";
        myData->tauCorrMC = "(1./(0.162191*(1-0.638512*(TMath::Erf((@0-29.883258)/14.960442)+1))))/6.184249";
    }


    //myData->customCut = "inclusiveTrueBd == 1.0 && jpsiParent_PDG==511";
    //myData->customCut = "";

    RooFitResult *result;
    RooFitResult *resultSimple;

    RooRealVar *mass = new RooRealVar(myData->massVarName, myData->massVarName, myData->massMin, myData->massMax);
    RooRealVar *time = new RooRealVar(myData->timeVarName,myData->timeVarName , myData->tauMin, myData->tauMax);
    RooRealVar *massErr = new RooRealVar(myData->massErrVarName, myData->massErrVarName, myData->massErrMin, myData->massErrMax);
    RooRealVar *timeErr = new RooRealVar(myData->timeErrVarName, myData->timeErrVarName, myData->tauErrMin, myData->tauErrMax);
    RooRealVar *pt = new RooRealVar(myData->ptVarName, myData->ptVarName, myData->ptMin, myData->ptMax);
    RooRealVar *weight = new RooRealVar("w", "w", myData->wMin,myData->wMax);
    RooArgSet*  vars = new RooArgSet(*mass, *time, *massErr, *timeErr, *pt, *myData->getYearsCategory(),*weight);

    RooDataSet* wdata = NULL;    
    RooDataSet* data = NULL; 

    if(!toyMC){
        if(!readRooData){
            myData->createData(useTriggers,true);
        }
        data = myData->readData(year);
        data->Print();
    }

    if(useWeight){
        wdata = new RooDataSet(data->GetName(),data->GetTitle(),data,*data->get(),0,weight->GetName()) ;
        delete data;
    }
    else wdata = data;
 
    if (sample > 0) {
    Long64_t nOrig = wdata->numEntries();
    TString title = wdata->GetTitle();
    title += " Random sample ( ";
    title += sample;
    title += " / ";
    title += nOrig;
    title += " )";
    RooDataSet *newDataset = new RooDataSet("wdataRandomSample", title, *vars);
    TRandom3 rand(0);
    for ( Long64_t i = 0; i < sample; i++ ) {
        Long64_t j = rand.Uniform( 0 , nOrig );
        newDataset->add( *(wdata->get( j )) );
    }
    cout << "INFO: Created random sample (" << sample << " from " << nOrig << ")" << endl;
    wdata=newDataset;
    }
    TH1F* TH_signalPunzi     = (TH1F*)wdata->createHistogram("TH_signalPunzi",*timeErr,Binning(100,myData->tauErrMin, myData->tauErrMax));
    RooDataHist* signal_dataHist     = new RooDataHist("signal_dataHist",     "signal_dataHist",     *timeErr,   TH_signalPunzi);
    RooHistPdf* signalPunziPDF     = new RooHistPdf("signalPunziPDF",    "signalPunziPDF",    *timeErr, *signal_dataHist    );
        
    RooRealVar *timeResolution_mean               = new RooRealVar   ("timeResolution_mean", "Mean (0.0) of Time Resolution", 0.);
    RooRealVar *timeResolution_SF = new RooRealVar   ("timeResolution_SF", "SF of Time Error", 1.0, 0.8, 1.5);
    RooGaussModel *timeResolutionPDF  = new RooGaussModel("timeResolutionPDF", "Lifetime Resolution Gaussian (for both Sig and Bck)", *time, *timeResolution_mean,*timeResolution_SF,*timeErr);

    RooRealVar *timeResolution_SF2 = new RooRealVar   ("timeResolution_SF2", "SF of Time Error", 3.0, 1.5, 4.);
    RooGaussModel *timeResolutionPDF2  = new RooGaussModel("timeResolutionPDF2", "Lifetime Resolution Gaussian (for both Sig and Bck)", *time, *timeResolution_mean,*timeResolution_SF2,*timeErr);
    RooRealVar *fraction = new RooRealVar   ("fraction", "fraction", 0.9, 0.01, 0.99);
    
    RooRealVar *timeResolution_SF3 = new RooRealVar   ("timeResolution_SF3", "SF of Time Error", 6.0, 4.0, 10.);
    RooGaussModel *timeResolutionPDF3  = new RooGaussModel("timeResolutionPDF3", "Lifetime Resolution Gaussian (for both Sig and Bck)", *time, *timeResolution_mean,*timeResolution_SF3,*timeErr);
    RooRealVar *fraction2 = new RooRealVar   ("fraction2", "fraction", 0.1, 0.001, 0.99);
    
    //RooAddModel* resolution = new RooAddModel("resolution","resolution",RooArgList(*timeResolutionPDF,*timeResolutionPDF2,*timeResolutionPDF3),RooArgSet(*fraction,*fraction2));
    RooAddModel* resolution = new RooAddModel("resolution","resolution",RooArgList(*timeResolutionPDF,*timeResolutionPDF2),RooArgSet(*fraction));

     ////Signal
    //Time
    RooRealVar *timeSignal_tau = new RooRealVar("timeSignal_tau","timeSignal_tau",1.53,1.3,1.7) ;
    RooDecay* timeSignalPDF =  new RooDecay("signalTime","signalTime",*time,*timeSignal_tau,*resolution,RooDecay::SingleSided) ;
    RooProdPdf* timeSignalPDFCond = new RooProdPdf("timeSignalPDFCond","timeSignalPDFCond",*signalPunziPDF,Conditional(*timeSignalPDF,*time)) ;
    
    //ConditionalObservables(*punziParams)
    if(useWeight)result = timeSignalPDFCond->fitTo(*wdata,ConditionalObservables(*timeErr),Extended(kFALSE),Save(),NumCPU(2),SumW2Error(kTRUE), PrefitDataFraction(0.05), BatchMode(true));
    else result = timeSignalPDFCond->fitTo(*wdata,ConditionalObservables(*timeErr),Extended(kFALSE),Save(),NumCPU(2), PrefitDataFraction(0.05), BatchMode(true));
    //if(useWeight)result = timeSignalPDF->fitTo(*wdata,Extended(kFALSE),Save(),NumCPU(2),SumW2Error(kTRUE), PrefitDataFraction(0.05), BatchMode(true));
    //else result = timeSignalPDF->fitTo(*wdata,Extended(kFALSE),Save(),NumCPU(2), PrefitDataFraction(0.05), BatchMode(true));


    //Print Results of Punzi and Mass prefit
    TCanvas *c = new TCanvas("c","",2*800,600);
    TPad* pads[4];
    for(int i = 0;i<2;i++){
        pads[i] = new TPad(Form("pad%d",6+i), "",i*0.5,1.0,0.5*(i+1),0.2);
        pads[2+i] = new TPad(Form("pad%d",9+i), "",i*0.5,0.2,0.5*(i+1),0.0);
    }
    for(int i = 0;i<4;i++)pads[i]->Draw();

    auto timeSt = std::time(nullptr);
    auto timeStLocal = *std::localtime(&timeSt);
    std::ostringstream timeStText;
    timeStText << std::put_time(&timeStLocal, "%d-%m-%Y_%H:%M:%S");

    TString outFigureName = "BdMC.png";

    double mainBinSize = 0.24;
   double fineBinSize = 0.06;
   double peakWindow = 0.5;
   RooBinning tbins(myData->tauMin, myData->tauMax);
   tbins.addUniform(TMath::Nint(fabs(-1*peakWindow-myData->tauMin)/mainBinSize), myData->tauMin, -1*peakWindow);
   tbins.addUniform(TMath::Nint(2*peakWindow/fineBinSize), -1*peakWindow, peakWindow);
   tbins.addUniform(TMath::Nint(fabs(myData->tauMax-peakWindow)/mainBinSize),peakWindow,  myData->tauMax);

    RooPlot* timeErrFrame = (RooPlot*)timeErr->frame();
    RooPlot* timeFrame = (RooPlot*)time->frame();
    time->setRange("zoom",-0.2,0.2);
    RooPlot* timeZoomFrame = (RooPlot*)time->frame(Range("zoom"));

    wdata->plotOn(timeErrFrame,DataError(RooAbsData::SumW2));
    timeSignalPDFCond->plotOn(timeErrFrame);
    wdata->plotOn(timeFrame,DataError(RooAbsData::SumW2),Binning(tbins));
    timeSignalPDFCond->plotOn(timeFrame,Binning(tbins));
    wdata->plotOn(timeZoomFrame,DataError(RooAbsData::SumW2));
    timeSignalPDFCond->plotOn(timeZoomFrame);

    RooHist* hpullTime = timeFrame->pullHist() ;
    RooPlot* timeFramePull = (RooPlot*)time->frame() ;
    timeFramePull->addPlotable(hpullTime,"P") ;

    RooHist* hpullTimeZoom = timeZoomFrame->pullHist() ;
    RooPlot* timeZoomFramePull = (RooPlot*)time->frame(Range("zoom")) ;
    timeZoomFramePull->addPlotable(hpullTimeZoom,"P") ;
    
    RooPolynomial* pol0= new RooPolynomial("pol0","pol0",*time,RooArgList());
    pol0->plotOn(timeFramePull,LineColor(kRed));
    pol0->plotOn(timeZoomFramePull,LineColor(kRed));

    pads[0]->cd();
    pads[0]->SetLogy();
    timeFrame->SetMinimum(1.0);
    timeFrame->Draw();
    pads[1]->cd();
    pads[1]->SetLogy();
    //timeErrFrame->Draw();
    timeZoomFrame->SetMinimum(1e1);
    timeZoomFrame->Draw();
    pads[2]->cd();
    timeFramePull->Draw();
    pads[3]->cd();
    timeZoomFramePull->Draw();
    c->SaveAs(outFigureName);

    //resultSimple->Print("v");
    result->Print("v");

    cout << "Total number of events:" <<wdata->numEntries() <<endl;
       
}
