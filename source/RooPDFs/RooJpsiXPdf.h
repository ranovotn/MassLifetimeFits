#include "RooAbsPdf.h"
#include "RooAbsReal.h"
#include "RooRealProxy.h"


#ifndef RooJpsiXPdf_h
	#define RooJpsiXPdf_h


	class RooJpsiXPdf : public RooAbsPdf
	{
		public:
			RooJpsiXPdf()
			{
			};

			RooJpsiXPdf(const char *name, const char *title, RooAbsReal &_x, RooAbsReal &_m0, RooAbsReal &_s);
			RooJpsiXPdf(const RooJpsiXPdf &other, const char *name = 0);

			virtual TObject *clone(const char *newname) const
			{
				return new RooJpsiXPdf(*this, newname);
			};

			inline virtual ~RooJpsiXPdf()
			{
			};


			virtual Int_t getAnalyticalIntegral(RooArgSet &allVars, RooArgSet &analVars, const char *rangeName = 0) const;
			virtual Double_t analyticalIntegral(Int_t code, const char *rangeName = 0) const;


		protected:
			RooRealProxy x;
			RooRealProxy m0;
			RooRealProxy s;


			Double_t evaluate() const;


		private:
#ifdef __CINT__ /* HACK */
			ClassDef(RooJpsiXPdf, 1);
#endif /* __CINT__ HACK */
	};

#endif
