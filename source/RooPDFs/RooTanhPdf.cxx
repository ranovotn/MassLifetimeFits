#include "RooTanhPdf.h" 


#include "TIterator.h"
#include "TMath.h"

//#ifdef __CINT__ /* HACK */
ClassImp(RooTanhPdf)
//#endif /* __CINT__ HACK */


RooTanhPdf::RooTanhPdf(const char *name, const char *title, RooAbsReal &_x, RooAbsReal &_s, RooAbsReal &_o) : RooAbsPdf(name, title), x("x", "x", this, _x), s("s", "s", this, _s), o("o", "o", this, _o)
{
};

RooTanhPdf::RooTanhPdf(const RooTanhPdf &other, const char *name) : RooAbsPdf(other, name), x("x", this, other.x), s("s", this, other.s), o("o", this, other.o)
{
};


Double_t RooTanhPdf::evaluate() const
{
	return(TMath::TanH(s * (x - o)) + 1.0);
};


Int_t RooTanhPdf::getAnalyticalIntegral(RooArgSet &allVars, RooArgSet &analVars, const char *rangeName) const
{
	if(true == matchArgs(allVars, analVars, x))
	{
		return(1);
	}
	else
	{
		return(0);
	};
};

Double_t RooTanhPdf::analyticalIntegral(Int_t code, const char *rangeName) const
{
	const Double_t xMax = x.max(rangeName);
	const Double_t xMin = x.min(rangeName);

	const Double_t UpperLimit = (TMath::Log(TMath::CosH(s * (xMax - o))) / s) + xMax;
	const Double_t LowerLimit = (TMath::Log(TMath::CosH(s * (xMin - o))) / s) + xMin;

	const Double_t Integral = UpperLimit - LowerLimit;
	return(Integral);
};
