/*****************************************************************************
 * Project: RooFit                                                           *
 * Package: RooFitModels                                                     *
 *    File: $Id: RooDSCB.h,v 1.11 2007/07/12 20:30:49 wouter Exp $
 * Authors:                                                                  *
 *   WV, Wouter Verkerke, UC Santa Barbara, verkerke@slac.stanford.edu       *
 *   DK, David Kirkby,    UC Irvine,         dkirkby@uci.edu                 *
 *                                                                           *
 * Copyright (c) 2000-2005, Regents of the University of California          *
 *                          and Stanford University. All rights reserved.    *
 *                                                                           *
 * Redistribution and use in source and binary forms,                        *
 * with or without modification, are permitted according to the terms        *
 * listed in LICENSE (http://roofit.sourceforge.net/license.txt)             *
 *****************************************************************************/
#ifndef ROO_DSCB
#define ROO_DSCB

#include "RooAbsPdf.h"
#include "RooAbsReal.h"
#include "RooRealProxy.h"

class RooRealVar;

class RooDSCB : public RooAbsPdf {
public:
  RooDSCB() {} ;
  RooDSCB(const char *name, const char *title, RooAbsReal& _m,
             RooAbsReal& _m0, RooAbsReal& _sigma,
             RooAbsReal& _alphaL, RooAbsReal& _alphaR, RooAbsReal& _n);

  RooDSCB(const RooDSCB& other, const char* name = 0);
  virtual TObject* clone(const char* newname) const { return new RooDSCB(*this,newname); }

  inline virtual ~RooDSCB() { };

//   virtual Int_t getAnalyticalIntegral( RooArgSet& allVars,  RooArgSet& analVars, const char* rangeName=0 ) const;
//   virtual Double_t analyticalIntegral( Int_t code, const char* rangeName=0 ) const;

  // Optimized accept/reject generator support
//   virtual Int_t getMaxVal(const RooArgSet& vars) const ;
//   virtual Double_t maxVal(Int_t code) const ;

protected:

  Double_t ApproxErf(Double_t arg) const ;

  RooRealProxy m;
  RooRealProxy m0;
  RooRealProxy sigma;
  RooRealProxy alphaL;
  RooRealProxy alphaR;
  RooRealProxy n;

  Double_t evaluate() const;

private:
    
//#ifdef __CINT__ /* HACK */
ClassDef(RooDSCB,1) // Crystal Ball lineshape PDF
//#endif /* __CINT__ HACK */
};

#endif
 

