#ifndef ROOATLAS
#define ROOATLAS

#include "RooAbsPdf.h"
#include "RooAbsReal.h"
#include "RooRealProxy.h"

//---

class RooATLAS : public RooAbsPdf
{
public:
  RooATLAS(const char *name, const char *title,
	   RooAbsReal &_x,
	   RooAbsReal &_x0, RooAbsReal &_N, RooAbsReal &_A,
	   RooAbsReal &_P4, RooAbsReal &_P3, RooAbsReal &_P2, RooAbsReal &_P1, RooAbsReal &_P0
	   );
  RooATLAS(const RooATLAS &other, const char *name = 0);

  virtual TObject *clone(const char *newname) const
  {
    return new RooATLAS(*this, newname);
  };

  inline virtual ~RooATLAS()
  {
  };


protected:
  RooRealProxy x;

  RooRealProxy x0;
  RooRealProxy N;
  RooRealProxy A;

  RooRealProxy P4;
  RooRealProxy P3;
  RooRealProxy P2;
  RooRealProxy P1;
  RooRealProxy P0;


  Double_t evaluate() const;


private:
#ifdef __CINT__ /* HACK */
  ClassDef(RooATLAS, 1);
#endif /* __CINT__ HACK */
};

#endif
