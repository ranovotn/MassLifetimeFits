 #include "RooRealVar.h"
 #include "RooDataSet.h"
 #include "RooDataHist.h"
 #include "RooGaussian.h"
 #include "RooGaussModel.h"
// #include "RooCrystalBall.h"
 #include "RooExponential.h"
 #include "TCanvas.h"
 #include "RooPlot.h"
 #include "TTree.h"
 #include "TFile.h"
 #include "TH1D.h"
 #include "TRandom.h"
 #include "RooAddPdf.h"
 #include "RooDecay.h"
 #include "RooProdPdf.h"
 #include "TH1F.h"
 #include "TH2F.h"
 #include "RooHistPdf.h"
 #include "RooFormulaVar.h"
 #include "RooProduct.h"
 #include "RooCategory.h"
// #include "RooPDFs/RooTanhPdf.h"
// #include "RooPDFs/RooTanhPdf.cxx"
 #include "TRandom3.h"
 #include "RooHist.h"
 #include "RooFitResult.h"
 #include "RooWorkspace.h"
 #include <unistd.h>
#include "Class/MyData.h"
#include "Class/MyData.cxx"

#include "PunziSplot/lifetime_splot.h"
#include "PunziSplot/lifetime_splot.cxx"


// #include "RooPDFs/RooDSCB.h"
// #include "RooPDFs/RooDSCB.cxx"

R__LOAD_LIBRARY(../source/RooPDFs/RooTanhPdf.cxx++);
R__LOAD_LIBRARY(../source/RooPDFs/RooDSCB.cxx++);

using namespace RooFit;

void BplusMassLifetime(TString year = "",bool useWeight = true, bool readRooData = true,int sample = -1, bool toyMC = false){
    RooWorkspace* workspace = new RooWorkspace("workspace",kTRUE) ;
    MyData *myData = new MyData("myData","/eos/atlas/atlascerngroupdisk/phys-beauty/BLifetime/BPlus/data/March2022/*.root");

    myData->mainTree = "BplusBestChi";
    myData->trigTree = "BplusTriggers";
    myData->rooFitFile = "../data/RooDatasetBp.root";

    myData->massPDG = 5279.34;
    myData->massMin = 5000;
    myData->massMax = 5600;
    myData->tauMin = -2;
    myData->tauMax = 23.0;
    myData->tauErrMin = .01;
    myData->tauErrMax = .25;
    myData->massErrMin = 10;
    myData->massErrMax = 150;
    myData->ptMin = 10;
    myData->ptMax = 150;
    myData->wMin = 0;
    myData->wMax = 100;

    myData->customCut = "B_mu1_pT>6000 && B_mu2_pT>6000";


    myData->triggers2015 = "(HLT_2mu4_bJpsimumu_noL2 == 1 || HLT_mu6_mu4_bBmumuxv2 == 1 || HLT_mu18_2mu0noL1_JpsimumuFS == 1)";
    myData->triggers2016A= "((HLT_2mu6_bBmumuxv2 == 1) || (HLT_mu6_mu4_bBmumuxv2 == 1) || (HLT_mu10_mu6_bBmumuxv2 == 1))";
    myData->triggers2016B= "((HLT_2mu6_bBmumuxv2 == 0) && (HLT_mu6_mu4_bBmumuxv2 == 0) && (HLT_mu10_mu6_bBmumuxv2 == 0)) && (HLT_mu20_2mu0noL1_JpsimumuFS == 1 || HLT_mu10_mu6_bJpsimumu == 1 || HLT_mu6_mu4_bJpsimumu == 1)";
    myData->triggers2016C= "(HLT_2mu6_bJpsimumu_delayed == 1 || HLT_2mu6_bBmumuxv2_delayed == 1 || HLT_mu6_mu4_bBmumuxv2_delayed==1 || HLT_2mu4_bJpsimumu_L1BPH_2M8_2MU4 == 1 || HLT_mu6_mu4_bJpsimumu_delayed == 1 || HLT_2mu6_bBmumuxv2 == 1 || HLT_mu6_mu4_bJpsimumu == 1 || HLT_mu20_2mu4_JpsimumuL2 == 1)";
    myData->triggers2017=  "( HLT_2mu6_bJpsimumu_L1BPH_2M9_2MU6_BPH_2DR15_2MU6 == 1 || HLT_2mu6_bBmumuxv2_L1LFV_MU6 == 1 || HLT_mu11_mu6_bDimu == 1 || HLT_mu11_mu6_bBmumuxv2 == 1 || HLT_mu20_2mu2noL1_JpsimumuFS == 1 || HLT_mu6_mu4_bBmumuxv2 == 1 || HLT_2mu14 == 1 || HLT_mu50 == 1 || HLT_mu6_mu4_bBmumux_BsmumuPhi_L1BPH_2M9_MU6MU4_BPH_0DR15_MU6MU4 == 1 || HLT_2mu4_bBmumux_BsmumuPhi_L1BPH_2M9_2MU4_BPH_0DR15_2MU4 == 1 || HLT_mu22_mu8noL1 == 1 || HLT_mu11_2mu4noL1_bNocut_L1MU11_2MU6 == 1 || HLT_mu20_2mu4_JpsimumuL2 == 1 )";
    myData->triggers2018=  "( HLT_2mu6_bJpsimumu_L1BPH_2M9_2MU6_BPH_2DR15_2MU6 == 1 || HLT_2mu6_bBmumuxv2_L1LFV_MU6 == 1 || HLT_mu11_mu6_bDimu == 1 || HLT_mu11_mu6_bBmumuxv2 == 1 || HLT_mu20_2mu2noL1_JpsimumuFS == 1 || HLT_mu6_mu4_bBmumuxv2 == 1 || HLT_2mu14 == 1 || HLT_mu50 == 1 || HLT_mu6_mu4_bBmumux_BsmumuPhi_L1BPH_2M9_MU6MU4_BPH_0DR15_MU6MU4 == 1 || HLT_2mu4_bBmumux_BsmumuPhi_L1BPH_2M9_2MU4_BPH_0DR15_2MU4 == 1 || HLT_mu22_mu8noL1 == 1 || HLT_mu11_2mu4noL1_bNocut_L1MU11_2MU6 == 1 || HLT_mu20_2mu4_JpsimumuL2 == 1 )";

    myData->tauCorr2015 = "(1./(0.274407*(1-0.421709*(TMath::Erf((@0-27.291111)/10.825437)+1))))/3.644778";
    myData->tauCorr2016A= "(1./(0.241034*(1-0.414092*(TMath::Erf((@0-8.870874)/9.252832)+1))))/4.473236";
    myData->tauCorr2016B= "(1./(0.088420*(1-0.444709*(TMath::Erf((@0--51.742706)/12.429203)+1))))/102.273208";
    myData->tauCorr2016C= "(1./(0.224168*(1-0.418187*(TMath::Erf((@0-26.470765)/10.425898)+1))))/4.461548";
    myData->tauCorr2017 = "(1./(0.237452*(1-0.453662*(TMath::Erf((@0-27.049979)/12.475258)+1))))/4.215516";
    myData->tauCorr2018 = "(1./(0.237022*(1-0.472117*(TMath::Erf((@0-27.725697)/13.288633)+1))))/4.225342";  

    bool useTriggers = true;
    bool fixSecondNegative = true;
    bool backgroundPunziSubtractMass =false;
    bool backgroundPunziSubtractTime =false;
    bool fixSignalMass = false;
    bool fixJpsiXmass = true;
    bool plotFigures = true;
    Double_t sidebandCutoff = 150;//was 180 can be 130?
    Double_t signaCutoff = 50;
    
    cout << "fixSignalMass: "<< fixSignalMass<<endl;
    cout << "fixJpsiXmass: "<< fixJpsiXmass<<endl;
    cout << "fixSecondNegative: "<< fixSecondNegative<<endl;
    cout << "plotFigures: "<< plotFigures<<endl;
    cout << "Custom cuts used: "<<myData->customCut <<endl;

    int nBins = 40;
    int nCPU = 8;
    //READ the data from file and put them to the RooDataset
    RooFitResult *result;
    RooFitResult *resultSimple;
    RooRealVar *mass = new RooRealVar("B_mass", "B_mass", myData->massMin, myData->massMax);
    RooRealVar *time = new RooRealVar("B_tau_MinA0", "B_tau_MinA0", myData->tauMin, myData->tauMax);
    RooRealVar *massErr = new RooRealVar("B_mass_err", "B_mass_err", myData->massErrMin, myData->massErrMax);
    RooRealVar *timeErr = new RooRealVar("B_tau_MinA0_err", "B_tau_MinA0_err", myData->tauErrMin, myData->tauErrMax);
    RooRealVar *pt = new RooRealVar("B_pT", "B_pT", myData->ptMin, myData->ptMax);
    RooRealVar *weight = new RooRealVar("w", "w", myData->wMin,myData->wMax);
    RooArgSet*  vars = new RooArgSet(*mass, *time, *massErr, *timeErr, *pt, *myData->getYearsCategory(),*weight);
    //RoArgSet*  varsToy = new RooArgSet(*mass, *time, *massErr, *timeErr, *pt,*yearsCategory);

    RooDataSet* wdata = NULL;    
    RooDataSet* data = NULL; 

    if(!toyMC){
        if(!readRooData){
            myData->createData(true);
        }
        data = myData->readData(year);
        data->Print();
    }
    else{
        TFile* resultsFile =  new TFile("BPlusMassLifetimeFitResults.root");
        workspace = (RooWorkspace*)resultsFile->Get("workspace");
        RooAbsPdf *toyPDF = workspace->pdf("totPDF");
        data = (RooDataSet*)toyPDF->generate(*vars,3258110);
    }

    if(useWeight){
        wdata = new RooDataSet(data->GetName(),data->GetTitle(),data,*data->get(),0,weight->GetName()) ;
        delete data;
    }
    else wdata = data;
 
    if (sample > 0) {
    Long64_t nOrig = wdata->numEntries();
    TString title = wdata->GetTitle();
    title += " Random sample ( ";
    title += sample;
    title += " / ";
    title += nOrig;
    title += " )";
    RooDataSet *newDataset = new RooDataSet("wdataRandomSample", title, *vars);
    TRandom3 rand(0);
    for ( Long64_t i = 0; i < sample; i++ ) {
        Long64_t j = rand.Uniform( 0 , nOrig );
        newDataset->add( *(wdata->get( j )) );
    }
    cout << "INFO: Created random sample (" << sample << " from " << nOrig << ")" << endl;
    wdata=newDataset;
    }
    
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////    
//// MASS model --------------------------------------------------------------------------------------------------------------
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//Signal - 2 gaussain pdfs----------------------------------------------------------------------------------------------------
    RooRealVar *mass_mean      = new RooRealVar ("massSignal_mean", "Mean of Gaussian", 5279,5200, 5350);
    RooRealVar *mass_sigma1    = new RooRealVar ("massSignal_sigma1", "Sigma1 of DoubleGaussian", 20, 1, 50);
    RooRealVar *mass_sigma2    = new RooRealVar ("massSignal_sigma2", "Sigma2 of DoubleGaussian",40, 1, 90);
    RooRealVar *mass_fraction  = new RooRealVar ("massSignal_fraction", "Fraction of DoubleGaussian",0.5, 0., 1.);
    RooGaussian *mass_gauss1   = new RooGaussian("massSignal_gauss1", "Gaussian 1", *mass, *mass_mean, *mass_sigma1);
    RooGaussian *mass_gauss2   = new RooGaussian("massSignal_gauss2", "Gaussian 2", *mass, *mass_mean, *mass_sigma2);
    RooAddPdf* mass_gauss = new RooAddPdf  ("massSignal_gauss", "Mass Signal DoubleGaussian", RooArgList(*mass_gauss1, *mass_gauss2), *mass_fraction, kTRUE);

//Signal - missreconstructed B+ -> J/psi pi as signal
    RooRealVar *mass_meanMR       = new RooRealVar ("massSignal_meanMR", "", 5358.577);
    RooRealVar *mass_sigmaMR      = new RooRealVar ("massSignal_sigmaMR", "", 51.873);
    RooRealVar *mass_alfaLMR      = new RooRealVar ("massSignal_alfaLMR", "", 1.4586 );
    RooRealVar *mass_alfaRMR      = new RooRealVar ("massSignal_alfaRMR", "", 1.2684);
    RooRealVar *mass_nMR          = new RooRealVar ("massSignal_nMR", "",  6.171);
    //RooCrystalBall *mass_missReco = new RooCrystalBall("mass_missReco","mass_missReco",*mass,*mass_meanMR,*mass_sigmaMR,*mass_alfaLMR,*mass_nMR,*mass_alfaRMR,*mass_nMR); //only in ROOT 6.24!!!
    RooDSCB *mass_missReco = new RooDSCB("massSignal_missReco","",*mass,*mass_meanMR,*mass_sigmaMR,*mass_alfaLMR,*mass_alfaRMR,*mass_nMR);

//Background - combinatoral background: exponential pdf
    RooRealVar *mass_slope1     = new RooRealVar    ("massBackground_slope1", "Slope of Exponential1",-8.83178e-04, -1, -1e-8);
    RooExponential *mass_CombinatorialBck = new RooExponential("massBackground_Combinatorial", "CombinatorialBck", *mass, *mass_slope1);

//    RooRealVar *mass_slope1     = new RooRealVar    ("massBackground_slope1", "Slope of Exponential1",-8.83178e-04, -1, -1e-8);
//    RooRealVar *mass_slope2     = new RooRealVar    ("massBackground_slope2", "Slope of Exponential2",-8.83178e-05, -1, -1e-8);
//    RooExponential *mass_CombinatorialBck1 = new RooExponential("massBackground_Combinatorial1", "CombinatorialBck1", *mass, *mass_slope1);
//    RooExponential *mass_CombinatorialBck2 = new RooExponential("massBackground_Combinatorial2", "CombinatorialBck2", *mass, *mass_slope2);
//    RooRealVar *mass_combinatorial_fraction = new RooRealVar ("mass_combinatorial_fraction", "Fraction of Exponentials",0.6, 0., 1.);
//    RooAddPdf *mass_CombinatorialBck = new RooAddPdf  ("massBackground_Combinatorial","mass combinatorial background",RooArgList(*mass_CombinatorialBck1,*mass_CombinatorialBck2),*mass_combinatorial_fraction,kTRUE);

//Background - JpsiX component
    RooRealVar *mass_scale   = new RooRealVar    ("mass_scale",  "Scale of Tanh",-0.0179206,-0.19,-0.0018);
    RooRealVar *mass_offset  = new RooRealVar    ("mass_offset", "Offset of Tanh", 5141.70,5141.70-40,5141.70+40);
    RooTanhPdf *mass_JpsiX   = new RooTanhPdf("mass_JpsiX",  "JpisX mass", *mass, *mass_scale, *mass_offset);

//Mass - combining models - for simple mass fit and SPlot
    RooRealVar *mass_fraction_missReco  = new RooRealVar ("mass_fraction_missReco", "Fraction of missreco in signal",0.0369);
    RooAddPdf* massSignalPDF = new RooAddPdf  ("massSignalPDF", "Mass Signal DoubleGaussian", RooArgList(*mass_missReco,*mass_gauss), *mass_fraction_missReco, kTRUE);

    RooRealVar *mass_bckFrac  = new RooRealVar ("mass_bckFrac", "mass_bckFrac",0.90, 0.1, 0.96);
    RooRealVar *mass_sigFrac  = new RooRealVar ("mass_sigFrac", "mass_sigFrac",0.44, 0., 1.);
    RooAddPdf* simpleMassPDF = new RooAddPdf  ("simpleMassPDF", "massPDF", RooArgList(*massSignalPDF,*mass_CombinatorialBck,*mass_JpsiX), RooArgList(*mass_sigFrac,*mass_bckFrac), kTRUE);

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////    
//// Creating Punzi --------------------------------------------------------------------------------------------------------------
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    bool useSplot = true;
    lifetime_splot* PunziClass = new lifetime_splot(mass,timeErr,pt,wdata,mass_gauss,mass_CombinatorialBck);
    PunziClass->BuildTotalPDF(true); //true for using the JpsiX component, otherwise the JpsiX component is not used (or it needs to be used in the constructor)
    RooArgList* punziParams= NULL;

    if(useSplot){
        punziParams = new RooArgList(*timeErr, *pt);
        PunziClass->RunSplot();
    }else{
        punziParams = new RooArgList(*timeErr);
        PunziClass->RunSBS();
    }
    PunziClass->PlotAndSaveResults("punziBplus.root","punziBplus.png");


    //RooArgList* punziParams= new RooArgList(*timeErr,*pt);

    RooDataHist* signal_dataHist     = new RooDataHist("signal_dataHist",     "signal_dataHist",     *punziParams,PunziClass->TH_signalPunzi    );
    RooDataHist* background_dataHist = new RooDataHist(" background_dataHist"," background_dataHist",*punziParams,PunziClass->TH_backgroundPunzi);
    RooDataHist* JpsiX_dataHist      = new RooDataHist("JpsiX_dataHist",      "JpsiX_dataHist",      *punziParams,PunziClass->TH_JpsiXPunzi     );

    RooHistPdf* signalPunziPDF     = new RooHistPdf("signalPunziPDF",    "signalPunziPDF",    *punziParams, *signal_dataHist    );
    RooHistPdf* backgroundPunziPDF = new RooHistPdf("backgroundPunziPDF","backgroundPunziPDF",*punziParams, *background_dataHist);
    RooHistPdf* JpsiXPunziPDF      = new RooHistPdf("JpsiXPunziPDF",     "JpsiXPunziPDF",     *punziParams, *JpsiX_dataHist     );

    //Updating JpsiX model from the fit used in SPlot class (tanh parameter are free there)
    mass_scale ->setVal(PunziClass->JpsiX_scale->getValV());
    mass_offset->setVal(PunziClass->JpsiX_offset->getValV());
    if(fixJpsiXmass){
        mass_offset->setConstant(true);
        mass_scale ->setConstant(true);
    }

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////    
//// Running standalone mass fit----------------------------------------------------------------------------------------------
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    resultSimple = simpleMassPDF->fitTo(*wdata,Extended(kFALSE),RooFit::SumW2Error(kTRUE),NumCPU(nCPU),Timer(kTRUE),Save());
    
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////    
//// LIFETIME model ----------------------------------------------------------------------------------------------------------
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//Time resolution
    RooRealVar *timeResolution_mean = new RooRealVar("timeResolution_mean", "Mean (0.0) of Time Resolution", 1.e-10);
    RooRealVar *timeResolution_SF   = new RooRealVar("timeResolution_SF",   "SF of Time Resolution error",   1.05, 0.7, 1.6);
    RooGaussModel *timeResolutionPDF1  = new RooGaussModel("timeResolutionPDF1", "Lifetime Resolution Gaussian (both Sig and Bck)", *time, *timeResolution_mean,*timeResolution_SF,*timeErr);

    RooRealVar *timeResolution_SF2 = new RooRealVar   ("timeResolution_SF2", "SF of Time Error", 3.0, 1.3, 3.0);
    RooGaussModel *timeResolutionPDF2  = new RooGaussModel("timeResolutionPDF2", "Lifetime Resolution Gaussian (for both Sig and Bck)", *time, *timeResolution_mean,*timeResolution_SF2,*timeErr);
    RooRealVar *resolutionFraction = new RooRealVar   ("resolutionFraction", "resolutionFraction", 0.9, 0.8, .98);

    RooRealVar *timeResolution_SF3 = new RooRealVar   ("timeResolution_SF3", "SF of Time Error", 5.0, 3., 10.);
    RooGaussModel *timeResolutionPDF3  = new RooGaussModel("timeResolutionPDF3", "Lifetime Resolution Gaussian (for both Sig and Bck)", *time, *timeResolution_mean,*timeResolution_SF3,*timeErr);
    RooRealVar *resolutionFraction2 = new RooRealVar   ("resolutionFraction2", "resolutionFraction2", 0.1, 0.01, .99);

    RooAddModel* timeResolutionPDF = new RooAddModel("timeResolutionPDF","resolution",RooArgList(*timeResolutionPDF1,*timeResolutionPDF2,*timeResolutionPDF3),RooArgSet(*resolutionFraction,*resolutionFraction2),kTRUE);   

//Signal
    RooRealVar *timeSignal_tau = new RooRealVar("timeSignal_tau","timeSignal_tau",1.639,1.5,1.8) ;
    RooDecay* timeSignalPDF =  new RooDecay("signalTime","signalTime",*time,*timeSignal_tau,*timeResolutionPDF,RooDecay::SingleSided) ;
    RooProdPdf* timeSignalPDF_cond = new RooProdPdf("timeSignalPDF_cond","timeSignalPDF_cond",*signalPunziPDF,Conditional(*timeSignalPDF,*time)) ;

//Background - combinatorial background
    RooRealVar *timeBck_tauPos1      = new RooRealVar("timeBck_tauPos1", "Bck Tau Pos 1", 1.40183, 0.05, 2.0);
    RooRealVar *timeBck_tauPos2      = new RooRealVar("timeBck_tauPos2", "Bck Tau Pos 2", 1.84001e-01, 0.05, 1.0);
    RooRealVar *timeBck_tauPos3      = new RooRealVar("timeBck_tauPos3", "Bck Tau Pos 3", 9.4001e-01, 0.05, 1.0);
    RooRealVar *timeBck_tauNeg       = new RooRealVar("timeBck_tauNeg",  "Bck Tau Neg",   1e-4  , 1e-8, 2.0);
    RooRealVar *timeBck_tauNeg2      = new RooRealVar("timeBck_tauNeg2",  "Bck Tau Neg2", 6.8e-2, 1e-8, 2.0);

    RooRealVar *timeBck_fractionPos1 = new RooRealVar("timeBck_fractionPos1", "Fraction of PositiveExpo1",6.27597e-01, 0., 1.);
    RooRealVar *timeBck_fractionPos2 = new RooRealVar("timeBck_fractionPos2", "Fraction of PositiveExpo2",2.30404e-01, 0., 1.);
    RooRealVar *timeBck_fractionPos3 = new RooRealVar("timeBck_fractionPos3", "Fraction of PositiveExpo3",2.30404e-01, 0., 1.);
    RooRealVar *timeBck_fractionNeg1 = new RooRealVar("timeBck_fractionNeg1", "Fraction of NegativeExpo", 0.786, 0.001, 0.90);
    RooRealVar *timeBck_fractionNeg2 = new RooRealVar("timeBck_fractionNeg2", "Fraction of NegativeExpo2",0.045, 0.001, 0.90);

    RooDecay *timeBck_decayPos1      = new RooDecay  ("timeBck_decayPos1", "Decay Pos 1", *time, *timeBck_tauPos1, *timeResolutionPDF, RooDecay::SingleSided);
    RooDecay *timeBck_decayPos2      = new RooDecay  ("timeBck_decayPos2", "Decay Pos 2", *time, *timeBck_tauPos2, *timeResolutionPDF, RooDecay::SingleSided);
    RooDecay *timeBck_decayPos3      = new RooDecay  ("timeBck_decayPos3", "Decay Pos 3", *time, *timeBck_tauPos3, *timeResolutionPDF, RooDecay::SingleSided);
    RooDecay *timeBck_decayNeg1      = new RooDecay  ("timeBck_decayNeg1", "Decay Neg 1", *time, *timeBck_tauNeg,  *timeResolutionPDF, RooDecay::Flipped);
    RooDecay *timeBck_decayNeg2      = new RooDecay  ("timeBck_decayNeg2", "Decay Neg 2", *time, *timeBck_tauNeg2, *timeResolutionPDF, RooDecay::Flipped);

    RooProdPdf* timeResolutionPDF_cond = new RooProdPdf("timeResolutionPDF_cond", "timeResolutionPDF_cond", *backgroundPunziPDF, Conditional(*timeResolutionPDF,*time) );
    RooProdPdf* timeBck_decayPos1_cond = new RooProdPdf("timeBck_decayPos1_cond", "timeBck_decayPos1_cond", *backgroundPunziPDF, Conditional(*timeBck_decayPos1,*time) );
    RooProdPdf* timeBck_decayPos2_cond = new RooProdPdf("timeBck_decayPos2_cond", "timeBck_decayPos2_cond", *backgroundPunziPDF, Conditional(*timeBck_decayPos2,*time) );
    RooProdPdf* timeBck_decayPos3_cond = new RooProdPdf("timeBck_decayPos3_cond","timeBck_decayPos3_cond",*backgroundPunziPDF,Conditional(*timeBck_decayPos3,*time)) ;
    RooProdPdf* timeBck_decayNeg1_cond = new RooProdPdf("timeBck_decayNeg1_cond", "timeBck_decayNeg1_cond", *backgroundPunziPDF, Conditional(*timeBck_decayNeg1,*time) );
    RooProdPdf* timeBck_decayNeg2_cond = new RooProdPdf("timeBck_decayNeg2_cond", "timeBck_decayNeg2_cond", *backgroundPunziPDF, Conditional(*timeBck_decayNeg2,*time) );

    RooAddPdf* timeBackgroundPDF_cond = new RooAddPdf ("timeBackgroundPDF_cond", "Lifetime Background (Prompt+2NegativeExpo+2PositiveExpo)", RooArgList(*timeResolutionPDF_cond,*timeBck_decayPos1_cond, *timeBck_decayPos2_cond,*timeBck_decayPos3_cond), RooArgList(*timeBck_fractionPos1, *timeBck_fractionPos2,*timeBck_fractionPos3), kTRUE);
 
    if(fixSecondNegative){
       timeBck_tauNeg2->setConstant(kTRUE);
       timeBck_fractionNeg2->setConstant(kTRUE);
    }

//Background - JpsiX component
    RooRealVar *timeJpsiX_tauPos1 = new RooRealVar("timeJpsiX_tauPos1",   "JpsiX Tau Pos Left", 1.82, 0.04, 4.0);
    RooDecay *timeJpsiX_decayPos1 = new RooDecay  ("timeJpsiX_decayPos1", "Decay Pos Left", *time, *timeJpsiX_tauPos1, *timeResolutionPDF, RooDecay::SingleSided);
    RooProdPdf* timeJpsiXPDF_cond = new RooProdPdf("timeJpsiXPDF_cond","timeBck_JpsiX_cond",*JpsiXPunziPDF,Conditional(*timeJpsiX_decayPos1,*time)) ;

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////    
//// MASS-LIFETIME model------------------------------------------------------------------------------------------------------
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//Signal
    RooProdPdf *signalPDF;
    signalPDF = new RooProdPdf("signalPDF","signalPDF",RooArgSet(*massSignalPDF,*timeSignalPDF_cond)); 

//Background - combinatorial
    RooProdPdf *backgroundPDF;
    backgroundPDF = new RooProdPdf("backgroundPDF","backgroundPDF",RooArgSet(*mass_CombinatorialBck,*timeBackgroundPDF_cond));

//Background - JpsiX component
    RooProdPdf *JpsiXPDF;
    JpsiXPDF = new RooProdPdf("JpsiXPDF","JpsiXPDF",RooArgSet(*mass_JpsiX,*timeJpsiXPDF_cond));

//Total model
    RooRealVar *sigFrac  = new RooRealVar ("sigFrac", "sigFrac",mass_sigFrac->getValV(), 0., 1.);
    RooRealVar *bckFrac  = new RooRealVar ("bckFrac", "bckFrac",mass_bckFrac->getValV(), 0., 1.);

    RooAddPdf* totPDF = new RooAddPdf  ("totPDF", "totPDF", RooArgList(*signalPDF, *backgroundPDF,*JpsiXPDF), RooArgList(*sigFrac,*bckFrac), kTRUE);
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////    
//// MASS-LIFETIME FIT------------------------------------------------------------------------------------------------------
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Setting constant from mass prefit
    if(fixSignalMass){
        mass_fraction->setConstant(kTRUE);
        mass_mean->setConstant(kTRUE);
        mass_sigma1->setConstant(kTRUE);
        mass_sigma2->setConstant(kTRUE);

        mass_slope1->setConstant(kTRUE);

        sigFrac->setConstant(kTRUE);
        bckFrac->setConstant(kTRUE);
    }

// Lifetime combinatorial background pre-fit
    RooDataSet* dataSidebandRight = (RooDataSet*)wdata->reduce(Form("B_mass>%f",mass_mean->getValV()+sidebandCutoff)); 
    cout << Form("B_mass>%f",mass_mean->getValV()+sidebandCutoff) << endl; 

    dataSidebandRight->SetName("dataSidebandRight");

    RooAddPdf* timeBackgroundPDF_rightSB = new RooAddPdf ("timeBackgroundPDF_cond_rightSB", "Lifetime Background (Prompt+2NegativeExpo+2PositiveExpo)",
                                                       RooArgList(*timeBck_decayPos1, *timeBck_decayPos2, *timeResolutionPDF),
                                                       RooArgList(*timeBck_fractionPos1, *timeBck_fractionPos2), kTRUE);

//    if(useWeight)resultRightSidebandLifetime = timeBackgroundPDF_rightSB->fitTo(*dataSidebandRight,Extended(kFALSE),Save(),RooFit::InitialHesse(kTRUE),NumCPU(nCPU),SumW2Error(kTRUE),BatchMode(kTRUE));
//    else resultRightSidebandLifetime = timeBackgroundPDF_rightSB->fitTo(*dataSidebandRight,Extended(kFALSE),Save(),RooFit::InitialHesse(kTRUE),NumCPU(nCPU),BatchMode(kTRUE));

    //timeBck_tauPos1->setConstant(kTRUE);
    //timeBck_tauPos2->setConstant(kTRUE);
    //timeBck_tauNeg->setConstant(kTRUE);
    //timeBck_tauNeg2->setConstant(kTRUE);


// Final fit ,ConditionalObservables(*punziParams)
    if(useWeight)result = totPDF->fitTo(*wdata,Extended(kFALSE),ConditionalObservables(*punziParams),Save(),NumCPU(nCPU),BatchMode(kTRUE),SumW2Error(kTRUE), PrefitDataFraction(0.05));
    else result = totPDF->fitTo(*wdata,ConditionalObservables(*punziParams),Extended(kFALSE),Save(),NumCPU(nCPU),BatchMode(kTRUE), PrefitDataFraction(0.05));

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////    
//// PLOTTING RESULTS------------------------------------------------------------------------------------------------------
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    if(plotFigures){
//Creating canvas and frames
    TCanvas *c = new TCanvas("c","",3*800,3*600);
    TPad* pads[12];
    for(int i = 0;i<3;i++){
        pads[i]   = new TPad(Form("pad%d",i),   "",i*0.3,1.0,0.3*(i+1),0.7);
        pads[3+i] = new TPad(Form("pad%d",3+i), "",i*0.3,0.7,0.3*(i+1),0.4);
        pads[6+i] = new TPad(Form("pad%d",6+i), "",i*0.3,0.4,0.3*(i+1),0.4*0.2);
        pads[9+i] = new TPad(Form("pad%d",9+i), "",i*0.3,0.4*0.2,0.3*(i+1),0.0);
    }
    for(int i = 0;i<12;i++)pads[i]->Draw();

    RooPlot* simpleMassFrame    = (RooPlot*)mass->frame();
    RooPlot* timeErrFrame = (RooPlot*)timeErr->frame();
    RooPlot* massErrFrame = (RooPlot*)massErr->frame();
    RooPlot* ptFrame = (RooPlot*)pt->frame();
    RooPlot* wFrame = (RooPlot*)weight->frame(Range(0.9,1.5));
    RooPlot* massFrame    = (RooPlot*)mass->frame();
    RooPlot* timeFrame = (RooPlot*)time->frame();
    RooPlot* timeRightSidebandFrame = (RooPlot*)time->frame();
    time->setRange("zoom",-0.2,0.2);
    RooPlot* timeZoomFrame = (RooPlot*)time->frame(Range("zoom"));
    RooAddPdf  *totPunziPdf = new RooAddPdf("totPunziPdf","totPunziPdf",RooArgList(*signalPunziPDF,*backgroundPunziPDF,*JpsiXPunziPDF),RooArgList(*sigFrac,*bckFrac),kTRUE);
    RooDataSet *expDataDterr = totPunziPdf->generate(*punziParams, 100000);
    /*RooRealVar* zeroConstant = new RooRealVar("zeroConstant","zeroConstant",0.0);
    RooPolynomial* pol0Time= new RooPolynomial("pol0Time","pol0Time",*time,RooArgList(*zeroConstant));
    RooPolynomial* pol0TimeZoom= new RooPolynomial("pol0TimeZoom","pol0Time",*time,RooArgList(*zeroConstant));
    RooPolynomial* pol0Mass= new RooPolynomial("pol0Mass","pol0Mass",*mass,RooArgList(*zeroConstant));*/
    RooGenericPdf* pol0Time = new RooGenericPdf("pol0Time","0",*time) ;
    RooGenericPdf* pol0Mass = new RooGenericPdf("pol0Mass","0",*mass) ;
    RooGenericPdf* pol0TimeZoom = new RooGenericPdf("pol0TimeZoom","0",*time) ;


   double mainBinSize = 0.24;
   double fineBinSize = 0.02;
   double peakWindow = 1.0;
   RooBinning tbins(myData->tauMin, myData->tauMax);
   tbins.addUniform(TMath::Nint(fabs(-1*peakWindow-myData->tauMin)/mainBinSize), myData->tauMin, -1*peakWindow);
   tbins.addUniform(TMath::Nint(2*peakWindow/fineBinSize), -1*peakWindow, peakWindow);
   tbins.addUniform(TMath::Nint(fabs(myData->tauMax-peakWindow)/mainBinSize),peakWindow,  myData->tauMax);


//Plotting simple mass
    wdata->plotOn(simpleMassFrame,DataError(RooAbsData::SumW2));
    simpleMassPDF->plotOn(simpleMassFrame,RooFit::Components("massSignal_gauss"),            LineColor(2));
    simpleMassPDF->plotOn(simpleMassFrame,RooFit::Components("massSignal_missReco"),         LineColor(6));
    simpleMassPDF->plotOn(simpleMassFrame,RooFit::Components("mass_JpsiX"),                  LineColor(1));
    simpleMassPDF->plotOn(simpleMassFrame,RooFit::Components("massBackground_Combinatorial"),LineColor(3));
    simpleMassPDF->plotOn(simpleMassFrame);

    pads[0]->cd();
    simpleMassFrame->Draw();

// Plotting time_err and pt punzi

    wdata->plotOn(timeErrFrame,DataError(RooAbsData::SumW2));
    totPunziPdf->plotOn(timeErrFrame,RooFit::Components("signalPunziPDF"),    LineColor(2));
    totPunziPdf->plotOn(timeErrFrame,RooFit::Components("backgroundPunziPDF"),LineColor(3));
    totPunziPdf->plotOn(timeErrFrame,RooFit::Components("JpsiXPunziPDF"),     LineColor(1));
    totPunziPdf->plotOn(timeErrFrame);

    pads[1]->cd();
    timeErrFrame->Draw();

    if(useSplot){
        wdata->plotOn(ptFrame,DataError(RooAbsData::SumW2));
        totPunziPdf->plotOn(ptFrame,RooFit::Components("signalPunziPDF"),    LineColor(2));
        totPunziPdf->plotOn(ptFrame,RooFit::Components("backgroundPunziPDF"),LineColor(3));
        totPunziPdf->plotOn(ptFrame,RooFit::Components("JpsiXPunziPDF"),     LineColor(1));
        totPunziPdf->plotOn(ptFrame);

        pads[2]->cd();
        ptFrame->Draw();
    }

//Plotting the lifetime fit in right sideband

    dataSidebandRight->plotOn(timeRightSidebandFrame,DataError(RooAbsData::SumW2));
    timeBackgroundPDF_cond->plotOn(timeRightSidebandFrame,LineColor(3),ProjWData(*expDataDterr, kTRUE));

    pads[3]->cd();
    pads[3]->SetLogy();
    timeRightSidebandFrame->SetMinimum(0.001);
    timeRightSidebandFrame->Draw();

//Plotting final mass

    wdata->plotOn(massFrame,DataError(RooAbsData::SumW2));
    totPDF->plotOn(massFrame,RooFit::Components("signalPDF"),            LineColor(2));
    totPDF->plotOn(massFrame,RooFit::Components("massSignal_missReco"),         LineColor(6));
    totPDF->plotOn(massFrame,RooFit::Components("backgroundPDF"),LineColor(3));
    totPDF->plotOn(massFrame,RooFit::Components("JpsiXPDF"),                  LineColor(1));
    totPDF->plotOn(massFrame);

    pads[6]->cd();
    massFrame->Draw();

    RooHist* hpullMass = massFrame->pullHist() ;
    RooPlot* massFramePull = (RooPlot*)mass->frame() ;
    massFramePull->addPlotable(hpullMass,"P") ;
    pol0Mass->plotOn(massFramePull,LineColor(kRed));

    pads[9]->cd();
    massFramePull->Draw();

//Plotting final lifetime 
    wdata->plotOn(timeFrame,DataError(RooAbsData::SumW2),Binning(tbins));
    totPDF->plotOn(timeFrame,RooFit::Components("signalPDF"),LineColor(2),ProjWData(*expDataDterr, kTRUE));
    totPDF->plotOn(timeFrame,ProjWData(*expDataDterr, kTRUE));

    pads[7]->cd();
    pads[7]->SetLogy();
    timeFrame->SetMinimum(1.0);
    timeFrame->Draw();
 
    RooHist* hpullTime = timeFrame->pullHist() ;
    RooPlot* timeFramePull = (RooPlot*)time->frame() ;
    timeFramePull->addPlotable(hpullTime,"P") ;
    timeFramePull->SetMinimum(-5.0);
    timeFramePull->SetMaximum(5.0);
    pol0Time->plotOn(timeFramePull,LineColor(kRed));

    pads[10]->cd();
    timeFramePull->Draw();

//Plotting final lifetime - zoomed

    wdata->plotOn(timeZoomFrame,DataError(RooAbsData::SumW2));
    totPDF->plotOn(timeZoomFrame,RooFit::Components("signalPDF"),LineColor(2),ProjWData(*expDataDterr, kTRUE));
    totPDF->plotOn(timeZoomFrame,ProjWData(*expDataDterr, kTRUE));

    pads[8]->cd();
    pads[8]->SetLogy();
    timeZoomFrame->SetMinimum(1.0);
    timeZoomFrame->Draw();

    RooHist* hpullTimeZoom = timeZoomFrame->pullHist() ;
    RooPlot* timeZoomFramePull = (RooPlot*)time->frame(Range("zoom")) ;
    timeZoomFramePull->addPlotable(hpullTimeZoom,"P") ;
    pol0TimeZoom->plotOn(timeZoomFramePull,LineColor(kRed));

    pads[11]->cd();
    timeZoomFramePull->Draw();


//Saving plots
    auto timeSt = std::time(nullptr);
    auto timeStLocal = *std::localtime(&timeSt);
    std::ostringstream timeStText;
    timeStText << std::put_time(&timeStLocal, "%d-%m-%Y_%H:%M:%S");

    if(year == "2016") c->SaveAs(Form("../run/BplusMassLifetime2016_%s.png",timeStText.str().c_str()));
    else if(year !="")c->SaveAs(Form("../run/BplusMassLifetime%s_%s.png",year.Data(),timeStText.str().c_str()));
    else c->SaveAs(Form("../run/BplusMassLifetimeFullRun2_%s.png",timeStText.str().c_str()));
    }
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////    
//// PRINTING RESULTS------------------------------------------------------------------------------------------------------
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//    resultSimple->Print("v");
//    resultRightSidebandLifetime->Print("v");
    result->Print("v");
    cout << "Total number of events:" <<wdata->numEntries() <<endl;

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////    
//// SAVING RESULTS AND MODELS INTO ROOT FILE-------------------------------------------------------------------------------
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    if(!toyMC){
	char buffer[PATH_MAX];
        getcwd(buffer, sizeof(buffer));

        TClass* RooTanhPdfClass = (TClass*)mass_JpsiX->Class();
        RooTanhPdfClass->SetDeclFile(Form("%s/../source/RooPDFs/RooTanhPdf.h",buffer), 0);
        RooTanhPdfClass->SetImplFileName(Form("%s/../source/RooPDFs/RooTanhPdf.cxx",buffer));

        TClass* RooDSCBClass = (TClass*)mass_missReco->Class();
        RooDSCBClass->SetDeclFile(Form("%s/../source/RooPDFs/RooDSCB.h",buffer), 0);
        RooDSCBClass->SetImplFileName(Form("%s/../source/RooPDFs/RooDSCB.cxx",buffer));
        workspace->addClassImplImportDir(Form("%s/../source/RooPDFs",buffer));
        workspace->addClassDeclImportDir(Form("%s/../source/RooPDFs",buffer));
        workspace->importClassCode(RooTanhPdfClass,kTRUE);
        workspace->importClassCode(RooDSCBClass,kTRUE);

        workspace->import(*totPDF);
        workspace->import(*wdata);
        workspace->Print("v");
        workspace->writeToFile("BplusMassLifetimeFitResults.root", kTRUE);
    }

//end of script
}//
