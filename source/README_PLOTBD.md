## Creating Bd plots

To create the plots for the lifetime fit of Bd run:
```
root -b -q PlotBd.C
```
This creates the histograms and saves them into the root file *output.root*. To draw the figures run:
```
root -b -q drawPlotBd.C
```

Both files contain the boolean **useConDis** to switch to unconstrained results. The older code that directly creates the plots is included as comments. A function to include systematics is also included (at the moment in comments).
