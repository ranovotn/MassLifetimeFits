#include "MyData.h"
#include "TChain.h"

MyData::MyData(const char *name,const char *inFilePath){
    this->name = name;
    this->inFilePath = inFilePath;
    yearsCategory = new RooCategory("yearsCategory","yearsCategory");
    yearsCategory->defineType("2015" ) ;
    yearsCategory->defineType("2016A") ;
    yearsCategory->defineType("2016B") ;
    yearsCategory->defineType("2016C") ;
    yearsCategory->defineType("2017" ) ;
    yearsCategory->defineType("2018" ) ;
}

RooCategory* MyData::getYearsCategory(){
    return this->yearsCategory;
}

RooDataSet * MyData::readPeriod(TString year, bool useTriggers){
    TChain* tree = new TChain(this->mainTree);
    tree->Add(this->inFilePath);
    TChain* treeTrig;

    if(year != "MC"){
        treeTrig = new TChain(this->trigTree);
        treeTrig->Add(this->inFilePath);
        tree->AddFriend(treeTrig);
    }

    RooRealVar *mass = new RooRealVar(massVarName, massVarName, massMin, massMax);
    RooRealVar *time = new RooRealVar(timeVarName, timeVarName, tauMin, tauMax);
    RooRealVar *massErr = new RooRealVar(massErrVarName, massErrVarName, massErrMin, massErrMax);
    RooRealVar *timeErr = new RooRealVar(timeErrVarName, timeErrVarName, tauErrMin, tauErrMax);
    RooRealVar *pt = new RooRealVar(ptVarName, ptVarName, ptMin, ptMax);


    TString newCuts = "";
     if(year != "MC"){
         newCuts = "( ( ( abs(B_mu1_eta)<=1.05 && abs(B_mu2_eta)<=1.05 )  && ( B_Jpsi_mass>2960 && B_Jpsi_mass<3228 ) ) || ( ( abs(B_mu1_eta)>1.05 && abs(B_mu1_eta)<=2.5 && abs(B_mu2_eta)>1.05 && abs(B_mu2_eta)<=2.5 )  && ( B_Jpsi_mass>2842 && B_Jpsi_mass<3346 ) ) || ( ( ( abs(B_mu1_eta)<=1.05 && abs(B_mu2_eta)>1.05 && abs(B_mu2_eta)<=2.5 ) || ( abs(B_mu1_eta)>1.05 && abs(B_mu1_eta)<=2.5 && abs(B_mu2_eta)<=1.05 ) )   && ( B_Jpsi_mass>2920 && B_Jpsi_mass<3269 ) ) )";
        newCuts += Form("&& (%s>%f && %s<%f)",timeVarName,tauMin,timeVarName,tauMax);
        newCuts += Form("&& (%s>%f && %s<%f)",timeErrVarName,tauErrMin,timeErrVarName,tauErrMax);
        newCuts += Form("&& (%s>%f && %s<%f)",massVarName,massMin,massVarName,massMax);
        newCuts += Form("&& (%s>%f && %s<%f)",massErrVarName,massErrMin,massErrVarName,massErrMax);
        newCuts += Form("&& (%s>%f && %s<%f)",ptVarName,ptMin*1000.0,ptVarName,ptMax*1000.0);
        newCuts += "&& (B_mu1_pT>4000 && B_mu2_pT>4000)";
        newCuts += "&& pass_GRL";
    }else{
        newCuts = "1==1";
    }
    
    if(customCut!=""){
        newCuts += "&&"+customCut;
    }

    if(year == "2015"){
        newCuts += "&& (run_number<284700)";
        yearsCategory->setLabel("2015");
    }  else if(year == "2016A"){
        newCuts += "&& (run_number<302737 && run_number >= 296400)";
        yearsCategory->setLabel("2016A");
    } else if(year == "2016B"){
        newCuts += " && (run_number<302737 && run_number >= 296400)";
        yearsCategory->setLabel("2016B");
    } else if(year == "2016C"){
         newCuts += " && (run_number>=302737 && run_number<=311481)";
         yearsCategory->setLabel("2016C");
    } else if(year == "2017"){
            newCuts += "&& (run_number>=324320 && run_number < 341649)";
            yearsCategory->setLabel("2017");
    }  else if(year == "2018"){
            newCuts += "&& (run_number>=348197)";
            yearsCategory->setLabel("2018");
    }
    if(useTriggers){
        if(year == "2015"){
            newCuts += "&& "+this->triggers2015;
        } else if(year == "2016A"){
             newCuts += "&& "+this->triggers2016A;
        } else if(year == "2016B"){
             newCuts += "&& "+this->triggers2016B;
        } else if(year == "2016C"){
             newCuts += " && "+this->triggers2016C;
        } else if(year == "2017"){
          newCuts += "&& "+this->triggers2017;
        } else if(year == "2018"){
           newCuts += "&& "+this->triggers2018;
        } else if(year == "MC"){
             newCuts += "&& "+this->triggersMC;
        }
    }
    cout << "Cuts used: "<< newCuts <<endl;

    Long64_t nEvents = tree->Draw("1", newCuts, "goff");
    tree->SetEstimate(nEvents);
    tree->Draw(Form("%s:%s:%s:%s:%s/1000", massVarName,timeVarName,massErrVarName,timeErrVarName,ptVarName), newCuts, "para goff");
    RooAbsData::setDefaultStorageType(RooAbsData::Tree);
    RooArgSet*  vars = new RooArgSet(*mass, *time, *massErr, *timeErr, *pt,*yearsCategory);
    RooDataSet* data = new RooDataSet("data", newCuts, *vars);
    TIterator *vars_it = vars->createIterator();
    for ( Long64_t i = 0; i < nEvents; i++ ) {
        vars_it->Reset();
        Int_t j = 0;
        for ( auto var = (RooRealVar*)vars_it->Next(); var != NULL ; var = (RooRealVar*)vars_it->Next() ) {
            if(j!=vars->getSize()-1)var->setVal(tree->GetVal(j)[i]);
            j++;
        }
        data->add(*vars);
    }
//add weights
        TString tauWeight = "1.0";
	if(year == "2015"){ tauWeight = this->tauCorr2015;
        } else if(year == "2016A"){tauWeight = this->tauCorr2016A;
        } else if(year == "2016B"){tauWeight = this->tauCorr2016B;
        } else if(year == "2016C"){tauWeight = this->tauCorr2016C;
        } else if(year == "2017"){tauWeight = this->tauCorr2017;
        }  else if(year == "2018"){tauWeight = this->tauCorr2018;
        }  else if(year == "MC"){tauWeight = this->tauCorrMC;
        }
        RooFormulaVar wFunc("w","event tau-weight",tauWeight,*time) ;
        RooRealVar* w = (RooRealVar*) data->addColumn(wFunc) ;

    cout << year<<": Total number of events:" <<data->numEntries() <<endl;
    data->Print();
    return data;
}

RooDataSet * MyData::createData(bool useTriggers,bool MC){
    RooAbsData::setDefaultStorageType(RooAbsData::Tree);
    RooDataSet* wdata = NULL;
    RooDataSet* wdataTmp[6];
    if(!MC){
        cout << "Use trigger selection: "<< useTriggers <<endl;
        cout << "Loading 2015..."<<endl;
        wdataTmp[0]=readPeriod("2015",useTriggers);
        cout << "Loading 2016A..."<<endl;
        wdataTmp[1]=readPeriod("2016A",useTriggers);
        cout << "Loading 2016B..."<<endl;
        wdataTmp[2]=readPeriod("2016B",useTriggers);
        cout << "Loading 2016C..."<<endl;
        wdataTmp[3]=readPeriod("2016C",useTriggers);
        cout << "Loading 2017..."<<endl;
        wdataTmp[4]=readPeriod("2017",useTriggers);
        cout << "Loading 2018..."<<endl;
        wdataTmp[5]=readPeriod("2018",useTriggers);
    
        wdata = wdataTmp[0];
        wdata->append(*wdataTmp[1]);
        wdata->append(*wdataTmp[2]);
        wdata->append(*wdataTmp[3]);
        wdata->append(*wdataTmp[4]);
        wdata->append(*wdataTmp[5]);
    }else{
        wdata = readPeriod("MC",useTriggers);
    }


    TFile* f = new TFile(this->rooFitFile,"RECREATE") ;
    wdata->Write();
    f->Close();

   return wdata;
}

RooDataSet* MyData::readData(TString year){
    TFile* f = new TFile(this->rooFitFile,"READ");
    RooDataSet* dataTmp = (RooDataSet*)f->Get("data"); 
    RooDataSet* dataResult = NULL;
    if(this->years.find(year) != this->years.end()){
        dataResult= (RooDataSet*) dataTmp->reduce(Form("yearsCategory==yearsCategory::%s",year.Data())) ;
    }else if(year == "2016"){
        dataResult= (RooDataSet*) dataTmp->reduce("yearsCategory==yearsCategory::2016A || yearsCategory==yearsCategory::2016C") ;
    }else if(year == "2015+2016"){
        dataResult= (RooDataSet*) dataTmp->reduce("yearsCategory==yearsCategory::2015 || yearsCategory==yearsCategory::2016A || yearsCategory==yearsCategory::2016C") ;
    }else if(year !="MC"){
        dataResult= (RooDataSet*) dataTmp->reduce("yearsCategory==yearsCategory::2015 || yearsCategory==yearsCategory::2016A || yearsCategory==yearsCategory::2016C || yearsCategory==yearsCategory::2017 || yearsCategory==yearsCategory::2018") ;
    }else return dataTmp;
    delete dataTmp;
    f->Close();
    return dataResult;
}
