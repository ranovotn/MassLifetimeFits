#ifndef MYPLOT
#define MYPLOT

#include "RooDataSet.h"
#include "../atlasrootstyle/AtlasStyle.C"
#include "../atlasrootstyle/AtlasUtils.C"
#include "../atlasrootstyle/AtlasLabels.C"


class MyPlot{
public:
    MyPlot(RooRealVar *var);
    ~MyPlot();

    TString name;
    RooRealVar *var;
    TCanvas *c;
    TPad* pads[2];
        
    RooPlot * plotVarAndPull(RooAbsData* data, RooAbsPdf*fit, TString outName, bool useWeight, int nBins, TString components[], TString data_legend);
    RooPlot * plotVarAndPullNice(RooAbsData* data, RooAbsPdf*fit, TString outName, bool useWeight, int nBins, TString components[], TString data_legend);
    RooPlot * plotVarAndPullNice2(RooAbsData* data, RooAbsPdf*fit, TString outName, bool useWeight, int nBins, TString components[], TString data_legend);
    RooPlot * plotVarAndResid(RooAbsData* data, RooAbsPdf*fit, TString outName, bool useWeight, int nBins, TString components[]);
    RooHist * getResid(RooAbsData* data, RooAbsPdf*fit, bool useWeight, int nBins);
    
};
#endif
