#ifndef MyDATA
#define MyDATA

class MyData{
public:
    MyData(const char *name,const char *inFilePath);
    ~MyData();

    const char * name;
    const char * inFilePath;
    const char * rooFitFile;
    TString cuts;
    const char* timeVarName ="B_tau_MinA0";
    const char* timeErrVarName ="B_tau_MinA0_err";
    const char* massVarName ="B_mass";
    const char* massErrVarName ="B_mass_err";
    const char* ptVarName ="B_pT"; 

    Double_t massPDG;
    Double_t massMin;
    Double_t massMax;
    Double_t tauMin;
    Double_t tauMax;
    Double_t tauErrMin;
    Double_t tauErrMax;
    Double_t massErrMin;
    Double_t massErrMax;
    Double_t ptMin;
    Double_t ptMax;
    Double_t wMin;
    Double_t wMax;

    TString mainTree;
    TString trigTree;

    TString triggers2015;
    TString triggers2016A;
    TString triggers2016B;
    TString triggers2016C;
    TString triggers2017;
    TString triggers2018;
    TString triggersMC;

    TString tauCorr2015;
    TString tauCorr2016A;
    TString tauCorr2016B;
    TString tauCorr2016C;
    TString tauCorr2017;
    TString tauCorr2018;
    TString tauCorrMC;
  
    TString customCut = "";

    set<TString> years {"2015","2016A", "2016B", "2016C", "2017", "2018"};
    RooCategory* yearsCategory;
    RooCategory* getYearsCategory();
    RooDataSet * readPeriod(TString year, bool useTriggers);
    RooDataSet * createData(bool useTriggers,bool MC = false);
    RooDataSet * readData(TString year);
};
#endif
