#ifndef LIFETIME_SPLOT
#define LIFETIME_SPLOT

#include "RooAbsPdf.h"
#include "RooRealVar.h"
#include "RooStats/SPlot.h"
#include "RooDataSet.h"
#include "RooRealVar.h"
#include "RooGaussian.h"
#include "RooExponential.h"
#include "RooChebychev.h"
#include "RooAddPdf.h"
#include "RooArgSet.h"
#include "RooArgList.h"

#include "RooProdPdf.h"
#include "RooAddition.h"
#include "RooProduct.h"
#include "TCanvas.h"
#include "RooAbsPdf.h"
#include "RooFit.h"
#include "RooFitResult.h"
#include "RooWorkspace.h"
#include "RooConstVar.h"

class lifetime_splot {
public:
    //! Constructor
    lifetime_splot(RooRealVar *mass,RooRealVar *time, RooRealVar *timeErr, RooRealVar *pt, RooDataSet *inputData, RooAbsPdf *massSigPDF,RooAbsPdf *massBckPDF_prompt,RooAbsPdf *massBckPDF_combinatorial, RooAbsPdf *massJpsiXPDF=NULL);

    //! Destructor
    ~lifetime_splot();

    void BuildTotalPDF(bool useJpsiXgenericPDF=true);
    void RunSplot();
    //void RunSBS();
    void PlotAndSaveResults(TString FileName = "punzi_timeErr_splot.root", TString PlotSaveName = "punzi_timeErr.png");

    RooHistPdf *signalPunziPDF=NULL;
    RooHistPdf *backgroundPunziPDF_prompt=NULL;
    RooHistPdf *backgroundPunziPDF_combinatorial=NULL;
    RooHistPdf *JpsiXPunziPDF=NULL;
    RooAddPdf  *TotPunziPDF=NULL;    

    RooRealVar *JpsiX_scale =NULL;
    RooRealVar *JpsiX_offset=NULL;

    TH1 *TH_signalPunzi=NULL;
    TH1 *TH_backgroundPunzi_prompt=NULL;
    TH1 *TH_backgroundPunzi_combinatorial=NULL;
    TH1 *TH_JpsiXPunzi=NULL;

    TH1 *TH_signalPunzi_timeErr=NULL;
    TH1 *TH_backgroundPunzi_timeErr_prompt=NULL;
    TH1 *TH_backgroundPunzi_timeErr_combinatorial=NULL;
    TH1 *TH_JpsiXPunzi_timeErr=NULL;


private:

    RooAbsPdf *m_massSigPDF=NULL;
    RooAbsPdf *m_massBckPDF=NULL;
    RooAbsPdf *m_massBckPDF_prompt=NULL;
    RooAbsPdf *m_massBckPDF_combinatorial=NULL;
    RooAbsPdf *m_totalPDF=NULL;
    RooAbsPdf *m_massJpsiXPDF=NULL;
    RooRealVar *m_promptFraction=NULL;

    RooRealVar *m_mass=NULL;
    RooRealVar *m_time=NULL;
    RooRealVar *m_timeErr=NULL;
    RooRealVar *m_pt=NULL;

    RooRealVar *m_signalYield=NULL;
    RooRealVar *m_bkgYield=NULL;
    RooRealVar *m_bkgYield_prompt=NULL;
    RooRealVar *m_bkgYield_combinatorial=NULL;
    RooRealVar *m_JpsiXYield=NULL;

    RooDataSet *m_data;
};


#endif
