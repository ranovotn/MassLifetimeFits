 #include "RooRealVar.h"
 #include "RooDataSet.h"
 #include "RooDataHist.h"
 #include "RooGaussian.h"
 #include "RooGaussModel.h"
 #include "RooExponential.h"
 #include "TCanvas.h"
 #include "RooPlot.h"
 #include "TTree.h"
 #include "TFile.h"
 #include "TH1D.h"
 #include "TRandom.h"
 #include "RooAddPdf.h"
 #include "RooDecay.h"
 #include "RooProdPdf.h"
 #include "TH1F.h"
 #include "TH2F.h"
 #include "RooHistPdf.h"
 #include "RooFormulaVar.h"
 #include "RooProduct.h"
 #include "TRandom3.h"
 #include "RooHist.h"
 #include "RooFitResult.h"
 #include "RooCategory.h"
 #include "RooWorkspace.h"
#include "TMath.h"

#include "core/MyData.cxx"
#include "core/MyPlot.cxx"

using namespace RooFit;
   
void syst_createWeightedDataset(TString resultsFileName = "BdMassLifetimeFitResultsjohnson_expexp_2018", TString weightName = "w",int nBins = 500, int sample = -1, bool nextIter = false){
    TString tag = "2018" ;
    TString outFigureName = "../run/syst_createWeightedDataset_"+tag ;

    auto timeSt = std::time(nullptr);
    auto timeInit = *std::localtime(&timeSt);
    std::ostringstream timeStText;
    timeStText << std::put_time(&timeInit, "%d-%m_%H-%M");
    //timeStText.str().c_str();

    ///////////////////Prepare Dataset///////////////////////////////
    //copy from Bd.MassLifetime.cxx
    ////////////////////////////////////////////////////////////////
    MyData *myData = new MyData("myData","/eos/atlas/atlascerngroupdisk/phys-beauty/BLifetime/BdKstar/data/January2022/*.root");
    myData->mainTree = "BdBestChi";
    myData->trigTree = "BdTriggers";
    myData->rooFitFile = "../data/RooDatasetBd.root";

    myData->massPDG = 5279.65;
    myData->massMin = 5000;
    myData->massMax = 5650;
    myData->tauMin = -1.5;
    myData->tauMax = 23;
    myData->tauErrMin = .01;
    myData->tauErrMax = .25;
    myData->massErrMin = 0.1;
    myData->massErrMax = 150;
    myData->ptMin = 10;
    myData->ptMax = 150;
    myData->wMin = 0;
    myData->wMax = 100;
    ////////////////////////////////////////////////////////////////
    RooArgSet*  vars;
//     RooDataSet* wdata = NULL;

    ////////////////Load Data andFit Results From File//////////////
    TFile* fileResults = new TFile("../run/"+resultsFileName+".root");
    RooWorkspace* workspace = (RooWorkspace*)fileResults->Get("workspace");
    RooDataSet* wdata = (RooDataSet*)workspace->data("data");

    RooAbsPdf *totPDF = workspace->pdf("totPDF");
    RooRealVar *mass =workspace->var("B_mass");
    RooRealVar *time =workspace->var("B_tau_MinA0");
    RooRealVar *massErr =workspace->var("B_mass_err");
    RooRealVar *timeErr =workspace->var("B_tau_MinA0_err");
    RooRealVar *pt =workspace->var("B_pT");
    vars= new RooArgSet(*mass, *time, *massErr, *timeErr, *pt);
    mass -> setBins(nBins);

    wdata->Print("v");
    int N = wdata->numEntries();
    std::cout<<N<<std::endl;
    sleep(10);

    //TODO
    //Print Results of Punzi and Mass prefit
    MyPlot *massPlot = new MyPlot(mass);

    TString components[2]={"massSignalPDF","massBckPDF"};

    auto massFrame = //massPlot->plotVarAndPullNice(data,totPDF, outFigureName+tag, true, nBins, components);
    massPlot->plotVarAndPullNice2(wdata,totPDF, outFigureName, true, nBins, components);
    auto residual = massFrame->residHist();

// //     cout<<(TString)  residual->ls()  <<endl;
    auto func = (TGraph*) massFrame->findObject("fit");
    auto dat = (TGraph*) massFrame->findObject("data");

        ////////////////////////Compute New Weights///////////////////////////////
    RooRealVar *pullFixingWeight = new RooRealVar("pullFixingWeight", "pullFixingWeight", myData->wMin,myData->wMax);
    RooRealVar *pullMessingWeight[4];
    pullMessingWeight[0]= new RooRealVar("pullMessingWeight_sin1", "pullMessingWeight_sin1", myData->wMin,myData->wMax);
    pullMessingWeight[1] = new RooRealVar("pullMessingWeight_sin5", "pullMessingWeight_sin5", myData->wMin,myData->wMax);
    pullMessingWeight[2] = new RooRealVar("pullMessingWeight_sin10", "pullMessingWeight_sin10", myData->wMin,myData->wMax);
    pullMessingWeight[3] = new RooRealVar("pullMessingWeight_gaus", "pullMessingWeight_gaus", myData->wMin,myData->wMax);

    vars->add(*pullFixingWeight);
    for (auto mw:pullMessingWeight) vars->add(*mw);

    RooDataSet* pullFixingWeightsDataset = new RooDataSet("pullFixingWeightsDataset", "pullFixingWeightsDataset_", *vars); //+data->GetTitle()

    long double  m, f, d,r ;
    long double www = 1.;
    cout<<myData->wMin<<myData->wMax<<endl;
    std::cout<<std::endl<<"Reweighting dataset:"<<std::endl;
    for ( Long64_t i = 0; i < N; i++ ) {
        if (i % 10000 == 0)    std::cout<<"\r" <<i<<" / "<<N<< std::flush;
        auto loader =  wdata->get(i);
        m = loader->getRealValue(mass->GetName());
        mass ->setVal(m);
        time ->setVal(loader->getRealValue(time->GetName()));
        massErr ->setVal(loader->getRealValue(massErr->GetName()));
        timeErr ->setVal(loader->getRealValue(timeErr->GetName()));
        pt ->setVal(loader->getRealValue(pt->GetName()));
        d = dat->Eval(m,0,"S");
        r = residual->Eval(m,0,"S");
        www = 1-r/d;
        pullFixingWeight->setVal(www);
        pullMessingWeight[0]->setVal((1+3/sqrt(d)*TMath::Sin(2*TMath::Pi()*(m-5000)/650))*www); //mass range 5000-5650 -> 65/130/650
        pullMessingWeight[1]->setVal((1+3/sqrt(d)*TMath::Sin(2*TMath::Pi()*(m-5000)/130))*www);
        pullMessingWeight[2]->setVal((1+3/sqrt(d)*TMath::Sin(2*TMath::Pi()*(m-5000)/65))*www);
        pullMessingWeight[3]->setVal((1-3/sqrt(d)*TMath::Gaus(m,5065,25)-3/sqrt(d)*TMath::Gaus(m,5460,60))); //1st bump 5040-5090; 2nd bump 5400-5520
        pullFixingWeightsDataset->add(*vars);
        }
    std::cout<<"\r" <<N<<" / "<<N<< std::endl;
    RooDataSet* wpullFixingWeightsDataset = new RooDataSet(pullFixingWeightsDataset->GetName(),pullFixingWeightsDataset->GetTitle(),pullFixingWeightsDataset,*pullFixingWeightsDataset->get(),0,pullFixingWeight->GetName()) ;
    massPlot->plotVarAndPull(wpullFixingWeightsDataset, totPDF, outFigureName+"fix", true, nBins, components);
//
    RooDataSet* wpullMessingWeightsDataset[4];
    RooDataHist* dataMessingBinned[4];
    for (int i=0; i<4; i++){
        wpullMessingWeightsDataset[i]= new RooDataSet((TString)pullFixingWeightsDataset->GetName()+Form("%d",i),pullFixingWeightsDataset->GetTitle(),pullFixingWeightsDataset,*pullFixingWeightsDataset->get(),0,pullMessingWeight[i]->GetName()) ;
        dataMessingBinned[i] = new RooDataHist((TString)"dataMessingBinned"+Form("%d",i), (TString)"binned version of d"+Form("%d",i), *mass, *wpullMessingWeightsDataset[i]);
        massPlot->plotVarAndPull(dataMessingBinned[i], totPDF, outFigureName+"mess"+Form("%d",i), true, nBins, components);
    }

    TFile *reFile = new TFile ((TString)"../data/sys_massPull_FixingWeights_"+Form("%d",nBins)+"_"+tag+".root","RECREATE") ;
    pullFixingWeightsDataset->Write();
    reFile->Close();

    auto timeFinal = *std::localtime(&timeSt);
    
    timeStText << std::put_time(&timeFinal, "%d-%m-%Y_%H:%M:%S");
    cout<<timeStText.str()<<endl;

    fileResults->Close() ;
}
