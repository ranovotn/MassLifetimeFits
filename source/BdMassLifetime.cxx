 #include "RooRealVar.h"
 #include "RooDataSet.h"
 #include "RooDataHist.h"
 #include "RooGaussian.h"
 #include "RooGaussModel.h"
 #include "RooExponential.h"
 #include "TCanvas.h"
 #include "RooPlot.h"
 #include "TTree.h"
 #include "TFile.h"
 #include "TH1D.h"
 #include "TRandom.h"
 #include "RooAddPdf.h"
 #include "RooDecay.h"
 #include "RooProdPdf.h"
 #include "TH1F.h"
 #include "TH2F.h"
 #include "RooHistPdf.h"
 #include "RooFormulaVar.h"
 #include "RooProduct.h"
 #include "TRandom3.h"
 #include "RooHist.h"
 #include "RooFitResult.h"
 #include "RooCategory.h"
 #include "RooWorkspace.h"
 
#include "core/MyData.h"
#include "core/MyData.cxx"
#include "core/lifetime_splot.h"
#include "core/lifetime_splot.cxx"

using namespace RooFit;
   
void BdMassLifetime(TString year = "",bool useWeight = true, bool readRooData = false,int sample = -1, bool toyMC = false){
    RooWorkspace* workspace = new RooWorkspace("workspace",kTRUE) ;
    MyData *myData = new MyData("myData","/eos/atlas/atlascerngroupdisk/phys-beauty/BLifetime/BdKstar/data/January2022/*.root");
    myData->mainTree = "BdBestChi";
    myData->trigTree = "BdTriggers";
    myData->rooFitFile = "../data/RooDatasetBd.root";

    myData->massPDG = 5279.65;
    myData->massMin = 5000;
    myData->massMax = 5650;
    myData->tauMin = -1.5;
    myData->tauMax = 23;
    myData->tauErrMin = .01;
    myData->tauErrMax = .25;
    myData->massErrMin = 0.1;
    myData->massErrMax = 150;
    myData->ptMin = 10;
    myData->ptMax = 150;
    myData->wMin = 0;
    myData->wMax = 100;

    myData->triggers2015 = "(HLT_2mu4_bJpsimumu_noL2 == 1 || HLT_mu6_mu4_bBmumuxv2 == 1 || HLT_mu18_2mu0noL1_JpsimumuFS == 1)";
    myData->triggers2016A= "((HLT_2mu6_bBmumuxv2 == 1) || (HLT_mu6_mu4_bBmumuxv2 == 1) || (HLT_mu10_mu6_bBmumuxv2 == 1))";
    myData->triggers2016B= "((HLT_2mu6_bBmumuxv2 == 0) && (HLT_mu6_mu4_bBmumuxv2 == 0) && (HLT_mu10_mu6_bBmumuxv2 == 0)) && (HLT_mu20_2mu0noL1_JpsimumuFS == 1 || HLT_mu10_mu6_bJpsimumu == 1 || HLT_mu6_mu4_bJpsimumu == 1)";
    myData->triggers2016C= "(HLT_2mu6_bJpsimumu_delayed == 1 || HLT_2mu6_bBmumuxv2_delayed == 1 || HLT_mu6_mu4_bBmumuxv2_delayed==1 || HLT_2mu4_bJpsimumu_L1BPH_2M8_2MU4 == 1 || HLT_mu6_mu4_bJpsimumu_delayed == 1 || HLT_2mu6_bBmumuxv2 == 1 || HLT_mu6_mu4_bJpsimumu == 1 || HLT_mu20_2mu4_JpsimumuL2 == 1)";
    myData->triggers2017=  "(HLT_2mu6_bJpsimumu_L1BPH_2M9_2MU6_BPH_2DR15_2MU6 == 1 || HLT_2mu6_bBmumuxv2_L1LFV_MU6 == 1 || HLT_mu11_mu6_bDimu == 1 || HLT_mu11_mu6_bBmumuxv2 ==1 || HLT_mu20_2mu2noL1_JpsimumuFS == 1 || HLT_mu6_mu4_bBmumuxv2 == 1 || HLT_2mu14 == 1 || HLT_mu50 == 1)";
    myData->triggers2018=  "(HLT_2mu6_bJpsimumu_L1BPH_2M9_2MU6_BPH_2DR15_2MU6 == 1 || HLT_2mu6_bBmumuxv2_L1LFV_MU6 == 1 || HLT_mu11_mu6_bDimu == 1 || HLT_mu11_mu6_bBmumuxv2 == 1 || HLT_mu20_2mu2noL1_JpsimumuFS == 1 || HLT_mu6_mu4_bBmumuxv2 == 1 || HLT_2mu14 == 1 || HLT_mu50 == 1 || HLT_mu22_mu8noL1 == 1 || HLT_mu20_2mu4_JpsimumuL2 == 1)";

    myData->tauCorr2015 = "(1./(0.160552*(1-0.654738*(TMath::Erf((@0-29.969762)/15.070683)+1))))/6.248619";
    myData->tauCorr2016A = "(1./(0.166868*(1-0.419287*(TMath::Erf((@0-9.806596)/8.177943)+1))))/6.227540";
    myData->tauCorr2016B = "(1./(0.033551*(1-0.438482*(TMath::Erf((@0--11051686859.594877)/1913.537036)+1))))/242.247150";
    myData->tauCorr2016C = "(1./(0.101746*(1-0.723472*(TMath::Erf((@0-30.166242)/15.963276)+1))))/9.882206";
    myData->tauCorr2017 = "(1./(0.079517*(1-33.827372*(TMath::Erf((@0-91.287130)/39.062252)+1))))/12.993400";
    myData->tauCorr2018 = "(1./(0.076658*(1-0.513180*(TMath::Erf((@0-24.738182)/12.913618)+1))))/13.090343"; 


    bool fixMass = false;
    bool fixSecondNegative = true;
    bool useTriggers = true;
    TString signalModel = "johnson";//dg, jj, jjfm
    TString bckModel    = "expexp";//expexp, pol3 // mass non prompt
    TString tag = signalModel+"_"+bckModel+"_"+year+"";

    int nBins = 100;
    int nCPU = 8;
    //READ the data from file and put them to the RooDataset
    RooFitResult *result;
    RooFitResult *resultSimple;

    RooRealVar *mass = new RooRealVar("B_mass", "B_mass", myData->massMin, myData->massMax);
    RooRealVar *time = new RooRealVar("B_tau_MinA0", "B_tau_MinA0", myData->tauMin, myData->tauMax);
    RooRealVar *massErr = new RooRealVar("B_mass_err", "B_mass_err", myData->massErrMin, myData->massErrMax);
    RooRealVar *timeErr = new RooRealVar("B_tau_MinA0_err", "B_tau_MinA0_err", myData->tauErrMin, myData->tauErrMax);
    RooRealVar *pt = new RooRealVar("B_pT", "B_pT", myData->ptMin, myData->ptMax);
    RooRealVar *weight = new RooRealVar("w", "w", myData->wMin,myData->wMax);
    RooArgSet*  vars = new RooArgSet(*mass, *time, *massErr, *timeErr, *pt, *myData->getYearsCategory(),*weight);

    RooDataSet* wdata = NULL;    
    RooDataSet* data = NULL; 

    if(!toyMC){
        if(!readRooData){
            myData->createData(true);
        }
        data = myData->readData(year);
        data->Print();
    }
    /*else{
        TFile* resultsFile =  new TFile("BdMassLifetimeFitResults.root");
        workspace = (RooWorkspace*)resultsFile->Get("workspace");
        RooAbsPdf *toyPDF = workspace->pdf("totPDF");
        data = (RooDataSet*)toyPDF->generate(*vars,3258110);
    }*/

    if(useWeight){
        wdata = new RooDataSet(data->GetName(),data->GetTitle(),data,*data->get(),0,weight->GetName()) ;
        delete data;
    }
    else wdata = data;
 
    if (sample > 0) {
    Long64_t nOrig = wdata->numEntries();
    TString title = wdata->GetTitle();
    title += " Random sample ( ";
    title += sample;
    title += " / ";
    title += nOrig;
    title += " )";
    RooDataSet *newDataset = new RooDataSet("wdataRandomSample", title, *vars);
    TRandom3 rand(0);
    for ( Long64_t i = 0; i < sample; i++ ) {
        Long64_t j = rand.Uniform( 0 , nOrig );
        newDataset->add( *(wdata->get( j )) );
    }
    cout << "INFO: Created random sample (" << sample << " from " << nOrig << ")" << endl;
    wdata=newDataset;
    }
    
    ///Prepare model
    ////Signal
    //Mass
    RooAbsPdf* massSignalPDF;
    RooRealVar *mass_mean, *mass_sigma1, *mass_sigma2, *mass_fraction,*mass_gamma,*mass_delta,*mass_lambda,*mass_mean2, *mass_gamma2,*mass_delta2, *mass_lambda2; 

    if(signalModel == "dg"){
        mass_mean = new RooRealVar ("massSignal_mean", "Mean of Gaussian", 5279,5200, 5350);
        mass_sigma1    = new RooRealVar ("massSignal_sigma1", "Sigma1 of DoubleGaussian", 20, 1, 30);
        mass_sigma2    = new RooRealVar ("massSignal_sigma2", "Sigma2 of DoubleGaussian",40, 1, 80);
        mass_fraction  = new RooRealVar ("massSignal_fraction", "Fraction of DoubleGaussian",0.5, 0., 1.);
        RooAbsPdf *mass_gauss1   = new RooGaussian("massSignal_gauss1", "Gaussian 1", *mass, *mass_mean, *mass_sigma1);
        RooAbsPdf *mass_gauss2   = new RooGaussian("massSignal_gauss2", "Gaussian 2", *mass, *mass_mean, *mass_sigma2);
        massSignalPDF = new RooAddPdf  ("massSignalPDF", "Mass Signal DoubleGaussian", RooArgList(*mass_gauss1, *mass_gauss2), *mass_fraction, kTRUE);
    }else if(signalModel == "johnson"){
        mass_mean = new RooRealVar ("massSignal_mean", "Mean of Gaussian", 5.2726e+03,5200, 5350);
        mass_gamma  = new RooRealVar ("massSignal_gamma", "", -3.1035e-01,-1.0,0.0);
        mass_delta  = new RooRealVar ("massSignal_delta", "", 1.3739e+00,0.3,10);
        mass_lambda = new RooRealVar ("massSignal_lambda", "", 3.0481e+01,10,100);

        massSignalPDF = new  RooJohnson("massSignalPDF", "Johnson Signal Decription", *mass, *mass_mean, *mass_lambda, *mass_gamma, *mass_delta);
    }else if(signalModel == "jj" || signalModel == "jjfm"){
        mass_mean = new RooRealVar ("massSignal_mean", "Mean of Gaussian", 5.2726e+03,5200, 5350);
        mass_gamma  = new RooRealVar ("massSignal_gamma", "", -3.1035e-01,-1.0,0.0);
        mass_delta  = new RooRealVar ("massSignal_delta", "", 1.3739e+00,0.3,10);
        mass_lambda = new RooRealVar ("massSignal_lambda", "", 3.0481e+01,10,100);
        
        mass_gamma2  = new RooRealVar ("massSignal_gamma2", "", 1.9883); //,0.5,10.0);
        mass_delta2  = new RooRealVar ("massSignal_delta2", "", 3.2868e+00,0.5,10);
        mass_lambda2 = new RooRealVar ("massSignal_lambda2", "", 7.3394e+01,10,200);

        RooJohnson *mass_johnson1 = new  RooJohnson("massSignal_johnson1", "Johnson Signal Decription", *mass, *mass_mean, *mass_lambda, *mass_gamma, *mass_delta);
        RooJohnson *mass_johnson2 = new  RooJohnson("massSignal_johnson2", "Johnson Signal Decription", *mass, *mass_mean, *mass_lambda2, *mass_gamma2, *mass_delta2);
        mass_fraction  = new RooRealVar ("massSignal_fraction", "Fraction of two Johnson functions",9.4603e-01, 0., 1.);

        massSignalPDF = new RooAddPdf("massSignalPDF","Double Johnson common mean",RooArgList(*mass_johnson1,*mass_johnson2),*mass_fraction, kTRUE);

    } else{
        cout << "Wrong signalModel:"<< signalModel <<endl;
        return; 
    }

    //// Combinatorial Background
    RooAbsPdf *massBckPDF_combinatorial;
    RooRealVar *massBck_slope1,*massBck_slope2, *massBck_fraction;
    RooRealVar *massBck_a1,*massBck_a2, *massBck_a3;
    if(bckModel=="expexp"){
        massBck_slope1     = new RooRealVar    ("massBck_slope1", "Slope of Exponential1",-6.83178e-04, -0.1, -1e-8);
        massBck_slope2     = new RooRealVar    ("massBck_slope2", "Slope of Exponential2",-9.83178e-04, -0.1, -1e-8);
        RooExponential *massBck_exponential1 = new RooExponential("massBck_exponential1", "massBck_exponential1", *mass, *massBck_slope1);
        RooExponential *massBck_exponential2 = new RooExponential("massBck_exponential2", "massBck_exponential2", *mass, *massBck_slope2);
        massBck_fraction  = new RooRealVar ("massBck_fraction", "Fraction of Two exponentials",0.5, 0., 1.);
        
        massBckPDF_combinatorial = new RooAddPdf  ("massBckPDF_combinatorial", "Mass Background DoubleGaussian", RooArgList(*massBck_exponential1, *massBck_exponential2),RooArgList(*massBck_fraction), kTRUE);
    }else if(bckModel=="pol3"){
        massBck_a1  = new RooRealVar ("massBck_a1", "a1",-3e-04, -0.001, 0.);
        massBck_a2  = new RooRealVar ("massBck_a2", "a2",0, -0.1, 0.1);
        massBck_a2->setConstant(kTRUE);
        massBck_a3  = new RooRealVar ("massBck_a3", "a3",-1e-05, -0.001, 0.);
        RooPolynomial *massBck_pol3 = new RooPolynomial("massBck_pol3", "massBck_pol3", *mass, RooArgList(*massBck_a1, *massBck_a2, *massBck_a3));

        massBckPDF_combinatorial = massBck_pol3;
    }else{
        cout << "Wrong backgroundModel:"<< bckModel <<endl;
        return; 
    }

    ///// Prompt background
    //RooRealVar* massBckPrompt_p0  = new RooRealVar ("massBckPrompt_p0", "",100, -1e12, 1e12);
    RooRealVar* massBckPrompt_p0  = new RooRealVar ("massBckPrompt_p0", "",-0.00013,-1e-3,-1e-12);
    RooRealVar* massBckPrompt_p1  = new RooRealVar ("massBckPrompt_p1", "",-2, -1e8, 1e8);
    RooRealVar* massBckPrompt_p2  = new RooRealVar ("massBckPrompt_p2", "",0.0002, -1e5, 1e5);

    RooPolynomial* massBckPDF_prompt = new RooPolynomial("massBckPDF_prompt","mass_pol",*mass,RooArgList(*massBckPrompt_p0));


    RooRealVar *promptFraction = new RooRealVar ("promptFraction", "Fraction of DoubleGaussian",0.6, 0.0, 1.);
    //Total
    //
    RooRealVar *mass_sigFrac  = new RooRealVar ("mass_sigFrac", "mass_sigFrac",0.22, 0.1, 0.96);    
    RooAddPdf* simpleMassPDF = new RooAddPdf  ("massPDF", "massPDF", RooArgList(*massSignalPDF, *massBckPDF_prompt,*massBckPDF_combinatorial),  RooArgList(*mass_sigFrac,*promptFraction),kTRUE );

    //standalone fit
    
    //RooDataHist* wdataBinned = new RooDataHist("wdataBinned", "binned version of d", *mass, *wdata);
    //resultSimple = simpleMassPDF->fitTo(*wdataBinned,RooFit::InitialHesse(kTRUE),RooFit::SumW2Error(kTRUE),NumCPU(nCPU),Timer(kTRUE),Save());
    // delete wdataBinned;
    
    //Sideband subtraction to prepare punzi
    bool useSplot = true;
    lifetime_splot* PunziClass = new lifetime_splot(mass,time,timeErr,pt,wdata,massSignalPDF,massBckPDF_prompt, massBckPDF_combinatorial);
    PunziClass->BuildTotalPDF(false); //true for using the JpsiX component, otherwise the JpsiX component is not used (or it needs to be used in the constructor)
    RooArgList* punziParams= NULL;

    if(useSplot){
        punziParams = new RooArgList(*timeErr, *pt);
        PunziClass->RunSplot();
    }
    PunziClass->PlotAndSaveResults("../run/punziBd"+tag+".root","../run/punziBd"+tag+".png");
    
    //Create Punzi PDF from the histograms

    RooDataHist* signal_dataHist     = new RooDataHist("signal_dataHist",     "signal_dataHist",     *punziParams,PunziClass->TH_signalPunzi    );
    RooDataHist* background_dataHist_prompt = new RooDataHist(" background_dataHist_prompt"," background_dataHist",*punziParams,PunziClass->TH_backgroundPunzi_prompt);
    RooDataHist* background_dataHist_combinatorial = new RooDataHist(" background_dataHist_combinatorial"," background_dataHist",*punziParams,PunziClass->TH_backgroundPunzi_combinatorial);

    RooHistPdf* signalPunziPDF     = new RooHistPdf("signalPunziPDF",    "signalPunziPDF",    *punziParams, *signal_dataHist    );
    RooHistPdf* backgroundPunziPDF_prompt = new RooHistPdf("backgroundPunziPDF_prompt","backgroundPunziPDF",*punziParams, *background_dataHist_prompt);
    RooHistPdf* backgroundPunziPDF_combinatorial = new RooHistPdf("backgroundPunziPDF_combinatorial","backgroundPunziPDF",*punziParams, *background_dataHist_combinatorial);
   
    ///Prepare mass-lifetimemodel    
    //Mass Resolution
    RooRealVar *massPCE_mean     = new RooRealVar   ("massPCE_mean", "Mean (0.0) of Mass Resolution", myData->massPDG,5250,5300);
    RooRealVar *massPCE_SF 	= new RooRealVar   ("massPCE_SF", "SF of Mass Error", 1.28, 0.1, 10.);
    RooProduct *massPCE_sigma       = new RooProduct("massPCE_sigma","massErr * Scale Factor", RooArgList(*massErr, *massPCE_SF));
    RooGaussian *massResolutionPDF  = new RooGaussian("massResolution", "Mass Resolution Gaussian (for both Sig and Bck)", *mass, *massPCE_mean,*massPCE_sigma);

    //Time resolution
    RooRealVar *timeResolution_mean               = new RooRealVar   ("timeResolution_mean", "Mean (0.0) of Time Resolution", 0.);//,-1.0,1.0);

    //RooRealVar *timeResolution_sigma = new RooRealVar   ("timeResolution_sigma", "SF of Time Error", 0.1, 1e-8, 10.);
    RooRealVar *timeResolution_SF1 = new RooRealVar   ("timeResolution_SF1", "SF of Time Error", 1, 0.7, 2.0);
    RooGaussModel *timeResolutionPDF1  = new RooGaussModel("timeResolutionPDF1", "Lifetime Resolution Gaussian (for both Sig and Bck)", *time, *timeResolution_mean,*timeResolution_SF1,*timeErr);
    
    RooRealVar *timeResolution_SF2 = new RooRealVar   ("timeResolution_SF2", "SF of Time Error", 1.7, 0.8, 3.);
    RooGaussModel *timeResolutionPDF2  = new RooGaussModel("timeResolutionPDF2", "Lifetime Resolution Gaussian (for both Sig and Bck)", *time, *timeResolution_mean,*timeResolution_SF2,*timeErr);
    RooRealVar *resolutionFraction = new RooRealVar   ("resolutionFraction", "resolutionFraction", 0.82, 0.7, .92);
    
    RooRealVar *timeResolution_SF3 = new RooRealVar   ("timeResolution_SF3", "SF of Time Error", 5.4, 3., 7.);
    RooGaussModel *timeResolutionPDF3  = new RooGaussModel("timeResolutionPDF3", "Lifetime Resolution Gaussian (for both Sig and Bck)", *time, *timeResolution_mean,*timeResolution_SF3,*timeErr);
    RooRealVar *resolutionFraction2 = new RooRealVar   ("resolutionFraction2", "resolutionFraction2", 0.16, 0.01, .3);

    RooAddModel* timeResolution = new RooAddModel("timeResolution","resolution",RooArgList(*timeResolutionPDF1,*timeResolutionPDF2,*timeResolutionPDF3),RooArgSet(*resolutionFraction,*resolutionFraction2),kTRUE);

 
     ////Signal
    //Time
    RooRealVar *timeSignal_tau = new RooRealVar("timeSignal_tau","timeSignal_tau",1.516,1.3,1.7) ;
    RooDecay* timeSignalPDF =  new RooDecay("signalTime","signalTime",*time,*timeSignal_tau,*timeResolution,RooDecay::SingleSided) ;
    RooProdPdf* timeSignalPDFCond = new RooProdPdf("timeSignalPDFCond","timeSignalPDFCond",*signalPunziPDF,Conditional(*timeSignalPDF,*time)) ;

    RooProdPdf *signalPDF = new RooProdPdf("signalPDF","signalPDF",RooArgSet(*massSignalPDF,*timeSignalPDFCond));
    ////Background
    //Mass

    //Time
    RooRealVar *timeBck_tauPos1      = new RooRealVar("timeBck_tauPos1", "Bck Tau Pos 1", 0.5, 1e-2, 1.0);
    RooRealVar *timeBck_tauPos2      = new RooRealVar("timeBck_tauPos2", "Bck Tau Pos 2", 3e-8, 1e-12, 1e-2);
    RooRealVar *timeBck_tauPos3      = new RooRealVar("timeBck_tauPos3", "Bck Tau Pos 3", 1.544, 1.0, 3.0);
    RooRealVar *timeBck_tauPos4      = new RooRealVar("timeBck_tauPos4", "Bck Tau Pos 4", 1.3970e-01, 1e-3, 1.0);
    RooRealVar *timeBck_tauPos5      = new RooRealVar("timeBck_tauPos5", "Bck Tau Pos 5", 7.4001e-01, 1e-5, 10.0);


    RooRealVar *timeBck_fractionPos1 = new RooRealVar("timeBck_fractionPos1", "Fraction of PositiveExpo1",1.9891e-01, 1e-3, 0.3);
    RooRealVar *timeBck_fractionPos2 = new RooRealVar("timeBck_fractionPos2", "Fraction of PositiveExpo2",3.2348e-01, 1e-3, 0.5);
    RooRealVar *timeBck_fractionPos3 = new RooRealVar("timeBck_fractionPos3", "Fraction of PositiveExpo3",4.1310e-01, 1e-4, 0.7);
    RooRealVar *timeBck_fractionPos4 = new RooRealVar("timeBck_fractionPos4", "Fraction of PositiveExpo4",2.30404e-01, 1e-4, 0.99);

    RooDecay *timeBck_decayPos1      = new RooDecay  ("timeBck_decayPos1", "Decay Pos 1", *time, *timeBck_tauPos1, *timeResolution, RooDecay::SingleSided);
    RooDecay *timeBck_decayPos2      = new RooDecay  ("timeBck_decayPos2", "Decay Pos 2", *time, *timeBck_tauPos2, *timeResolution, RooDecay::SingleSided);
    RooDecay *timeBck_decayPos3      = new RooDecay  ("timeBck_decayPos3", "Decay Pos 3", *time, *timeBck_tauPos3, *timeResolution, RooDecay::SingleSided);
    RooDecay *timeBck_decayPos4      = new RooDecay  ("timeBck_decayPos4", "Decay Pos 4", *time, *timeBck_tauPos4, *timeResolution, RooDecay::SingleSided);
    RooDecay *timeBck_decayPos5      = new RooDecay  ("timeBck_decayPos4", "Decay Pos 5", *time, *timeBck_tauPos5, *timeResolution, RooDecay::SingleSided);


    RooProdPdf* timeResolutionPDF_cond = new RooProdPdf("timeResolutionPDF_cond","timeBck_decayPos1_cond",*backgroundPunziPDF_prompt,Conditional(*timeResolution,*time)) ;
    RooProdPdf* timeBck_decayPos1_cond = new RooProdPdf("timeBck_decayPos1_cond","timeBck_decayPos1_cond",*backgroundPunziPDF_combinatorial,Conditional(*timeBck_decayPos1,*time)) ;
    RooProdPdf* timeBck_decayPos2_cond = new RooProdPdf("timeBck_decayPos2_cond","timeBck_decayPos2_cond",*backgroundPunziPDF_combinatorial,Conditional(*timeBck_decayPos2,*time)) ;
    RooProdPdf* timeBck_decayPos3_cond = new RooProdPdf("timeBck_decayPos3_cond","timeBck_decayPos3_cond",*backgroundPunziPDF_combinatorial,Conditional(*timeBck_decayPos3,*time)) ;
    RooProdPdf* timeBck_decayPos4_cond = new RooProdPdf("timeBck_decayPos4_cond","timeBck_decayPos4_cond",*backgroundPunziPDF_combinatorial,Conditional(*timeBck_decayPos4,*time)) ;
    RooProdPdf* timeBck_decayPos5_cond = new RooProdPdf("timeBck_decayPos5_cond","timeBck_decayPos5_cond",*backgroundPunziPDF_combinatorial,Conditional(*timeBck_decayPos5,*time)) ;
    RooAddPdf* timeBckPDF_combinatorialCond      = new RooAddPdf ("timeBckPDF_combinatorialCond", "Lifetime Background (Prompt+2NegativeExpo+2PositiveExpo)", RooArgList(*timeBck_decayPos1_cond,*timeBck_decayPos3_cond,*timeBck_decayPos4_cond), RooArgList(*timeBck_fractionPos1,*timeBck_fractionPos3), kTRUE);


    RooProdPdf *backgroundPDF_prompt = new RooProdPdf("backgroundPDF_prompt","backgroundPDF",RooArgSet(*massBckPDF_prompt,*timeResolutionPDF_cond));
    RooProdPdf *backgroundPDF_combinatorial = new RooProdPdf("backgroundPDF_combinatorial","backgroundPDF",RooArgSet(*massBckPDF_combinatorial,*timeBckPDF_combinatorialCond));
    //Total
    RooRealVar *sigFrac  = new RooRealVar ("sigFrac", "sigFrac",0.22, 0., 1.);
    RooAddPdf* totPDF = new RooAddPdf  ("totPDF", "totPDF", RooArgList(*signalPDF, *backgroundPDF_prompt,*backgroundPDF_combinatorial), RooArgList(*sigFrac,*promptFraction), kTRUE);


    RooMsgService::instance().setSilentMode(false);
    //Total fit
    if(fixMass){
        mass_mean->setConstant(kTRUE);
        mass_gamma->setConstant(kTRUE);
        mass_delta->setConstant(kTRUE);
        mass_lambda->setConstant(kTRUE);
        
        //mass_gamma2->setConstant(kTRUE);
        //mass_delta2->setConstant(kTRUE);
        //mass_lambda2->setConstant(kTRUE);
        //mass_fraction->setConstant(kTRUE);

        //massBck_fraction->setConstant(kTRUE);
        //massBck_slope1->setConstant(kTRUE);
        //massBck_slope2->setConstant(kTRUE);
    }
    //massBck_fraction->setVal(2.4476e-02);
    //massBck_slope1->setVal(-1.9610e-02);
    //massBck_slope2->setVal(-2.2963e-03);
    //,ConditionalObservables(*punziParams)
    if(useWeight)result = totPDF->fitTo(*wdata,Extended(kFALSE),ConditionalObservables(*punziParams),Save(),NumCPU(nCPU),SumW2Error(kTRUE), BatchMode(true),Offset(kTRUE));
    else result = totPDF->fitTo(*wdata,Extended(kFALSE),ConditionalObservables(*punziParams),Save(),NumCPU(nCPU), BatchMode(true),Offset(kTRUE));
    if(!toyMC){
        workspace->import(*totPDF); 
        workspace->import(*wdata);
        workspace->Print();
        workspace->writeToFile("../run/BdMassLifetimeFitResults"+tag+".root", kTRUE);
    }
    //Plot the data to the figure
    //DrawPunziPartofPDF

    //Print Results of Punzi and Mass prefit
    TCanvas *c = new TCanvas("c","",3*800,3*600);
    TPad* pads[12];
    for(int i = 0;i<3;i++){
        pads[i]   = new TPad(Form("pad%d",i),   "",i*0.3,1.0,0.3*(i+1),0.7);
        pads[3+i] = new TPad(Form("pad%d",3+i), "",i*0.3,0.7,0.3*(i+1),0.4);
        pads[6+i] = new TPad(Form("pad%d",6+i), "",i*0.3,0.4,0.3*(i+1),0.4*0.2);
        pads[9+i] = new TPad(Form("pad%d",9+i), "",i*0.3,0.4*0.2,0.3*(i+1),0.0);
    }
    for(int i = 0;i<12;i++)pads[i]->Draw();

    auto timeSt = std::time(nullptr);
    auto timeStLocal = *std::localtime(&timeSt);
    std::ostringstream timeStText;
    timeStText << std::put_time(&timeStLocal, "%d-%m-%Y_%H:%M:%S");

    TString outFigureName;
    if(year == "2016") outFigureName = Form("../run/BdMassLifetime2016_%s"+tag+".png",timeStText.str().c_str());
    else if(year !="")outFigureName = Form("../run/BdMassLifetime%s_%s"+tag+".png",year.Data(),timeStText.str().c_str());
    else outFigureName = Form("../run/BdMassLifetimeFullRun2_%s"+tag+".png",timeStText.str().c_str());

   double mainBinSize = 0.24;
   double fineBinSize = 0.02;
   double peakWindow = 1.0;
   RooBinning tbins(myData->tauMin, myData->tauMax);
   tbins.addUniform(TMath::Nint(fabs(-1*peakWindow-myData->tauMin)/mainBinSize), myData->tauMin, -1*peakWindow);
   tbins.addUniform(TMath::Nint(2*peakWindow/fineBinSize), -1*peakWindow, peakWindow);
   tbins.addUniform(TMath::Nint(fabs(myData->tauMax-peakWindow)/mainBinSize),peakWindow,  myData->tauMax);

    RooPlot* simpleMassFrame    = (RooPlot*)mass->frame();
    RooPlot* timeErrFrame = (RooPlot*)timeErr->frame();
    RooPlot* ptFrame = (RooPlot*)pt->frame();
    
    RooAddPdf  *totPunziPdf = new RooAddPdf("totPunziPdf","totPunziPdf",RooArgList(*signalPunziPDF,*backgroundPunziPDF_prompt,*backgroundPunziPDF_combinatorial),RooArgList(*sigFrac,*promptFraction),kTRUE);

    wdata->plotOn(simpleMassFrame,DataError(RooAbsData::SumW2));
    simpleMassPDF->plotOn(simpleMassFrame,RooFit::Components("massSignalPDF"),LineColor(2));
    simpleMassPDF->plotOn(simpleMassFrame,RooFit::Components("massBckPDF_prompt"),LineColor(1));
    simpleMassPDF->plotOn(simpleMassFrame,RooFit::Components("massBckPDF_combinatorial"),LineColor(3));
    simpleMassPDF->plotOn(simpleMassFrame);
    wdata->plotOn(timeErrFrame,DataError(RooAbsData::SumW2));
    totPunziPdf->plotOn(timeErrFrame);
    totPunziPdf->plotOn(timeErrFrame,RooFit::Components("signalPunziPDF"),LineColor(2));
    totPunziPdf->plotOn(timeErrFrame,RooFit::Components("backgroundPunziPDF_prompt"),LineColor(1));
    totPunziPdf->plotOn(timeErrFrame,RooFit::Components("backgroundPunziPDF_combinatorial"),LineColor(3));
    pads[0]->cd();
    simpleMassFrame->Draw();
    pads[1]->cd();
    timeErrFrame->Draw();
    if(useSplot){
        wdata->plotOn(ptFrame,DataError(RooAbsData::SumW2));
        totPunziPdf->plotOn(ptFrame);
        totPunziPdf->plotOn(ptFrame,RooFit::Components("signalPunziPDF"),LineColor(2));
        totPunziPdf->plotOn(ptFrame,RooFit::Components("backgroundPunziPDF_prompt"),LineColor(1));
        totPunziPdf->plotOn(ptFrame,RooFit::Components("backgroundPunziPDF_combinatorial"),LineColor(3));
        pads[2]->cd();
        ptFrame->Draw();
    }


    RooPlot* massFrame    = (RooPlot*)mass->frame();
    RooPlot* timeFrame = (RooPlot*)time->frame();
    time->setRange("zoom",-0.2,0.2);
    RooPlot* timeZoomFrame = (RooPlot*)time->frame(Range("zoom"));
    RooDataSet *expDataDterr = totPunziPdf->generate(RooArgSet(*timeErr,*pt), 100000);

    wdata->plotOn(massFrame,DataError(RooAbsData::SumW2));
    totPDF->plotOn(massFrame,RooFit::Components("signalPDF"),LineColor(2));
    totPDF->plotOn(massFrame,RooFit::Components("backgroundPDF_prompt"),LineColor(1));
    totPDF->plotOn(massFrame,RooFit::Components("backgroundPDF_combinatorial"),LineColor(3));
    totPDF->plotOn(massFrame);
    wdata->plotOn(timeFrame,DataError(RooAbsData::SumW2),Binning(tbins));
    totPDF->plotOn(timeFrame,RooFit::Components("signalPDF"),LineColor(2),ProjWData(*expDataDterr, kTRUE));
    totPDF->plotOn(timeFrame,RooFit::Components("backgroundPDF_prompt"),LineColor(1),ProjWData(*expDataDterr, kTRUE));
    totPDF->plotOn(timeFrame,RooFit::Components("backgroundPDF_combinatorial"),LineColor(3),ProjWData(*expDataDterr, kTRUE));
    totPDF->plotOn(timeFrame,ProjWData(*expDataDterr, kTRUE));
    wdata->plotOn(timeZoomFrame,DataError(RooAbsData::SumW2));
    totPDF->plotOn(timeZoomFrame,RooFit::Components("signalPDF"),LineColor(2),ProjWData(*expDataDterr, kTRUE));
    totPDF->plotOn(timeZoomFrame,RooFit::Components("backgroundPDF_prompt"),LineColor(1),ProjWData(*expDataDterr, kTRUE));
    totPDF->plotOn(timeZoomFrame,RooFit::Components("backgroundPDF_combinatorial"),LineColor(3),ProjWData(*expDataDterr, kTRUE));
    totPDF->plotOn(timeZoomFrame,ProjWData(*expDataDterr, kTRUE));

    RooHist* hpullMass = massFrame->pullHist() ;
    RooPlot* massFramePull = (RooPlot*)mass->frame() ;
    massFramePull->addPlotable(hpullMass,"P") ;

    RooHist* hpullTime = timeFrame->pullHist() ;
    RooPlot* timeFramePull = (RooPlot*)time->frame() ;
    timeFramePull->addPlotable(hpullTime,"P") ;
    timeFramePull->SetMinimum(-5.0);
    timeFramePull->SetMaximum(5.0);

    RooHist* hpullTimeZoom = timeZoomFrame->pullHist() ;
    RooPlot* timeZoomFramePull = (RooPlot*)time->frame(Range("zoom")) ;
    timeZoomFramePull->addPlotable(hpullTimeZoom,"P") ;

    //RooRealVar* zeroConstant = new RooRealVar("zeroConstant","zeroConstant",0.0);
    //RooPolynomial* pol0Time= new RooPolynomial("pol0Time","pol0Time",*time,RooArgList(*zeroConstant));
    //RooPolynomial* pol0TimeZoom= new RooPolynomial("pol0TimeZoom","pol0TimeZoom",*time,RooArgList(*zeroConstant));
    //RooPolynomial* pol0Mass= new RooPolynomial("pol0Mass","pol0Mass",*mass,RooArgList(*zeroConstant));
    RooGenericPdf* pol0Time = new RooGenericPdf("pol0Time","0",*time) ;
    RooGenericPdf* pol0Mass = new RooGenericPdf("pol0Mass","0",*mass) ;
    RooGenericPdf* pol0TimeZoom = new RooGenericPdf("pol0TimeZoom","0",*time) ;

    pol0Time->plotOn(timeFramePull,LineColor(kRed));
    pol0TimeZoom->plotOn(timeZoomFramePull,LineColor(kRed));
    pol0Mass->plotOn(massFramePull,LineColor(kRed));

    pads[6]->cd();
    massFrame->Draw();
    pads[7]->cd();
    pads[7]->SetLogy();
    timeFrame->SetMinimum(1.0);
    timeFrame->Draw();
    pads[8]->cd();
    pads[8]->SetLogy();
    timeZoomFrame->SetMinimum(1.0);
    timeZoomFrame->Draw();
    pads[9]->cd();
    massFramePull->Draw();
    pads[10]->cd();
    timeFramePull->Draw();
    pads[11]->cd();
    timeZoomFramePull->Draw();
    c->SaveAs(outFigureName);

    //resultSimple->Print("v");
    result->Print("v");
    TFile fileResults("../run/BdMassLifetimeFitResultsRoot"+tag+".root","RECREATE") ;
    result->Write("result") ;
    fileResults.Close() ;
    cout << "Total number of events:" <<wdata->numEntries() <<endl;
       
}
